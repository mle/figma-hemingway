import './ui.css';
import { format } from './hemingway.js';

function displayHtml(selector, value){
  document.querySelector(selector).innerHTML = value;
}

function getReadabilityIndex(characters, words, sentences) {
  // Automated readability index
  // https://en.wikipedia.org/wiki/Automated_readability_index
  return Math.round(
    4.71 * (characters / words) +
    0.5 * (words / sentences) -
    21.43
  );
}

function getReadingLevel(readabilityIndex: number) {
  if (readabilityIndex <= 5){
    return "Very easy";
  } else if (6 <= readabilityIndex && readabilityIndex < 8) {
    return "Easy";
  } else if (8 <= readabilityIndex && readabilityIndex < 12) {
    return "Standard";
  } else if (12 <= readabilityIndex && readabilityIndex < 14) {
    return "Difficult";
  } else if (14 <= readabilityIndex) {
    return "Very difficult";
  }
}

function displayData(data) {
  displayHtml("#readingLevel", `${getReadingLevel(getReadabilityIndex(data.characters, data.words, data.sentences))} reading level`);
  displayHtml("#counter", `${data.words} words, ${data.characters} characters`);
  displayHtml("#adverbCount", data.adverbs);
  displayHtml("#adverbLabel", `adverb${data.adverbs > 1 ? "s" : ""}, ${data.adverbs <= 2 ? "meeting goal" : "aim"} of 2 or fewer`);

  displayHtml("#passiveCount", data.passiveVoice);
  displayHtml("#passiveLabel", `use${data.passiveVoice > 1 ? "s" : ""} of passive voice, ${data.passiveVoice <= 2 ? "meeting goal" : "aim"} of 2 or fewer`);

  displayHtml("#complexCount", data.complex);
  displayHtml("#complexLabel", "phrases have a simpler alternatives");

  displayHtml("#hardSentenceCount", data.hardSentences);

  displayHtml("#veryHardSentenceCount", data.veryHardSentences);
}

onmessage = (event) => {
  const text = event.data.pluginMessage;

  const textOutput = document.getElementById('textOutput') as HTMLElement;

  // TODO process and highlight text and get Hemingway counts
  const analysis = format(text);
  textOutput.innerHTML = analysis.markup;
  displayData(analysis.stats);

  // Get height to resize the modal
  const container = document.getElementById('hemingway') as HTMLElement;
  const resizeHeight = Math.min(610, container.getBoundingClientRect().height + 40);
  parent.postMessage({ pluginMessage: { type: 'resize', height: resizeHeight } }, '*')
}
