const PLUGIN_WIDTH = 480;
const PLUGIN_HEIGHT = 600;

figma.ui.onmessage = msg => {
  if (msg.type === 'resize') {
    figma.ui.resize(PLUGIN_WIDTH, msg.height);
  }
}

function processText(textNode: TextNode) {
  figma.showUI(__html__, { visible: false, width: PLUGIN_WIDTH, height: PLUGIN_HEIGHT });

  figma.ui.postMessage(textNode.characters);

  figma.ui.show();
}

let currentSelection = figma.currentPage.selection;

if (currentSelection.length === 1 && currentSelection[0].type === 'TEXT'){
  processText(currentSelection[0]);
} else {
  figma.closePlugin("Please select a text node");
}
