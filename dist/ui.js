/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/ui.ts");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/css-loader/dist/cjs.js!./src/ui.css":
/*!**********************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/ui.css ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js")(false);
// Module
exports.push([module.i, "body {\n  font: 12px sans-serif;\n  margin: 8px 20px 20px;\n  overflow: hidden;\n}\n\n/* For stats */\n.characterStats {\n  display: flex;\n  justify-content: space-between;\n  align-items: center;\n  padding: 12px 24px;\n  border-top: 1px solid #e3e3e3;\n  margin: 0 -20px;\n}\n#readingLevel {\n  font-weight: bold;\n}\n\n#counter {\n  color: #828282;\n  text-align: right;\n}\n\n/* For Hemingway */\n.stat {\n  display: flex;\n  padding: 8px;\n  align-items: center;\n  border-radius: 4px;\n}\n\n.stat + .stat {\n  margin-top: 8px;\n}\n\n.stat .count {\n  background: #FFFFFF;\n  border-radius: 4px;\n  min-width: 20px;\n  padding: 4px;\n  text-align: center;\n}\n\n.stat .label {\n  margin-left: 8px;\n}\n\nspan {\n  padding: 2px;\n  border-radius: 4px;\n}\n\n.adverb {\n  background: #c4e3f3;\n}\n\n.qualifier {\n  background: #c4e3f3;\n}\n\n.passive {\n  background: #c4ed9d;\n}\n\n.complex {\n  background: #e3b7e8;\n}\n\n.hardSentence {\n  background: #f7ecb5;\n}\n\n.veryHardSentence {\n  background: #e4b9b9;\n}\n\n#textOutput {\n  max-height: 300px;\n  overflow: scroll;\n  margin: 0 -20px 20px;\n  padding: 0 20px;\n}\n\np {\n  -webkit-margin-before: 8px;\n  -webkit-margin-after: 8px;\n  font-size: 16px;\n  line-height: 1.4em;\n  color: #333333;\n}", ""]);


/***/ }),

/***/ "./node_modules/css-loader/dist/runtime/api.js":
/*!*****************************************************!*\
  !*** ./node_modules/css-loader/dist/runtime/api.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
// eslint-disable-next-line func-names
module.exports = function (useSourceMap) {
  var list = []; // return the list of modules as css string

  list.toString = function toString() {
    return this.map(function (item) {
      var content = cssWithMappingToString(item, useSourceMap);

      if (item[2]) {
        return "@media ".concat(item[2], "{").concat(content, "}");
      }

      return content;
    }).join('');
  }; // import a list of modules into the list
  // eslint-disable-next-line func-names


  list.i = function (modules, mediaQuery) {
    if (typeof modules === 'string') {
      // eslint-disable-next-line no-param-reassign
      modules = [[null, modules, '']];
    }

    var alreadyImportedModules = {};

    for (var i = 0; i < this.length; i++) {
      // eslint-disable-next-line prefer-destructuring
      var id = this[i][0];

      if (id != null) {
        alreadyImportedModules[id] = true;
      }
    }

    for (var _i = 0; _i < modules.length; _i++) {
      var item = modules[_i]; // skip already imported module
      // this implementation is not 100% perfect for weird media query combinations
      // when a module is imported multiple times with different media queries.
      // I hope this will never occur (Hey this way we have smaller bundles)

      if (item[0] == null || !alreadyImportedModules[item[0]]) {
        if (mediaQuery && !item[2]) {
          item[2] = mediaQuery;
        } else if (mediaQuery) {
          item[2] = "(".concat(item[2], ") and (").concat(mediaQuery, ")");
        }

        list.push(item);
      }
    }
  };

  return list;
};

function cssWithMappingToString(item, useSourceMap) {
  var content = item[1] || ''; // eslint-disable-next-line prefer-destructuring

  var cssMapping = item[3];

  if (!cssMapping) {
    return content;
  }

  if (useSourceMap && typeof btoa === 'function') {
    var sourceMapping = toComment(cssMapping);
    var sourceURLs = cssMapping.sources.map(function (source) {
      return "/*# sourceURL=".concat(cssMapping.sourceRoot).concat(source, " */");
    });
    return [content].concat(sourceURLs).concat([sourceMapping]).join('\n');
  }

  return [content].join('\n');
} // Adapted from convert-source-map (MIT)


function toComment(sourceMap) {
  // eslint-disable-next-line no-undef
  var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));
  var data = "sourceMappingURL=data:application/json;charset=utf-8;base64,".concat(base64);
  return "/*# ".concat(data, " */");
}

/***/ }),

/***/ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js":
/*!****************************************************************************!*\
  !*** ./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var stylesInDom = {};

var isOldIE = function isOldIE() {
  var memo;
  return function memorize() {
    if (typeof memo === 'undefined') {
      // Test for IE <= 9 as proposed by Browserhacks
      // @see http://browserhacks.com/#hack-e71d8692f65334173fee715c222cb805
      // Tests for existence of standard globals is to allow style-loader
      // to operate correctly into non-standard environments
      // @see https://github.com/webpack-contrib/style-loader/issues/177
      memo = Boolean(window && document && document.all && !window.atob);
    }

    return memo;
  };
}();

var getTarget = function getTarget() {
  var memo = {};
  return function memorize(target) {
    if (typeof memo[target] === 'undefined') {
      var styleTarget = document.querySelector(target); // Special case to return head of iframe instead of iframe itself

      if (window.HTMLIFrameElement && styleTarget instanceof window.HTMLIFrameElement) {
        try {
          // This will throw an exception if access to iframe is blocked
          // due to cross-origin restrictions
          styleTarget = styleTarget.contentDocument.head;
        } catch (e) {
          // istanbul ignore next
          styleTarget = null;
        }
      }

      memo[target] = styleTarget;
    }

    return memo[target];
  };
}();

function listToStyles(list, options) {
  var styles = [];
  var newStyles = {};

  for (var i = 0; i < list.length; i++) {
    var item = list[i];
    var id = options.base ? item[0] + options.base : item[0];
    var css = item[1];
    var media = item[2];
    var sourceMap = item[3];
    var part = {
      css: css,
      media: media,
      sourceMap: sourceMap
    };

    if (!newStyles[id]) {
      styles.push(newStyles[id] = {
        id: id,
        parts: [part]
      });
    } else {
      newStyles[id].parts.push(part);
    }
  }

  return styles;
}

function addStylesToDom(styles, options) {
  for (var i = 0; i < styles.length; i++) {
    var item = styles[i];
    var domStyle = stylesInDom[item.id];
    var j = 0;

    if (domStyle) {
      domStyle.refs++;

      for (; j < domStyle.parts.length; j++) {
        domStyle.parts[j](item.parts[j]);
      }

      for (; j < item.parts.length; j++) {
        domStyle.parts.push(addStyle(item.parts[j], options));
      }
    } else {
      var parts = [];

      for (; j < item.parts.length; j++) {
        parts.push(addStyle(item.parts[j], options));
      }

      stylesInDom[item.id] = {
        id: item.id,
        refs: 1,
        parts: parts
      };
    }
  }
}

function insertStyleElement(options) {
  var style = document.createElement('style');

  if (typeof options.attributes.nonce === 'undefined') {
    var nonce =  true ? __webpack_require__.nc : undefined;

    if (nonce) {
      options.attributes.nonce = nonce;
    }
  }

  Object.keys(options.attributes).forEach(function (key) {
    style.setAttribute(key, options.attributes[key]);
  });

  if (typeof options.insert === 'function') {
    options.insert(style);
  } else {
    var target = getTarget(options.insert || 'head');

    if (!target) {
      throw new Error("Couldn't find a style target. This probably means that the value for the 'insert' parameter is invalid.");
    }

    target.appendChild(style);
  }

  return style;
}

function removeStyleElement(style) {
  // istanbul ignore if
  if (style.parentNode === null) {
    return false;
  }

  style.parentNode.removeChild(style);
}
/* istanbul ignore next  */


var replaceText = function replaceText() {
  var textStore = [];
  return function replace(index, replacement) {
    textStore[index] = replacement;
    return textStore.filter(Boolean).join('\n');
  };
}();

function applyToSingletonTag(style, index, remove, obj) {
  var css = remove ? '' : obj.css; // For old IE

  /* istanbul ignore if  */

  if (style.styleSheet) {
    style.styleSheet.cssText = replaceText(index, css);
  } else {
    var cssNode = document.createTextNode(css);
    var childNodes = style.childNodes;

    if (childNodes[index]) {
      style.removeChild(childNodes[index]);
    }

    if (childNodes.length) {
      style.insertBefore(cssNode, childNodes[index]);
    } else {
      style.appendChild(cssNode);
    }
  }
}

function applyToTag(style, options, obj) {
  var css = obj.css;
  var media = obj.media;
  var sourceMap = obj.sourceMap;

  if (media) {
    style.setAttribute('media', media);
  }

  if (sourceMap && btoa) {
    css += "\n/*# sourceMappingURL=data:application/json;base64,".concat(btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))), " */");
  } // For old IE

  /* istanbul ignore if  */


  if (style.styleSheet) {
    style.styleSheet.cssText = css;
  } else {
    while (style.firstChild) {
      style.removeChild(style.firstChild);
    }

    style.appendChild(document.createTextNode(css));
  }
}

var singleton = null;
var singletonCounter = 0;

function addStyle(obj, options) {
  var style;
  var update;
  var remove;

  if (options.singleton) {
    var styleIndex = singletonCounter++;
    style = singleton || (singleton = insertStyleElement(options));
    update = applyToSingletonTag.bind(null, style, styleIndex, false);
    remove = applyToSingletonTag.bind(null, style, styleIndex, true);
  } else {
    style = insertStyleElement(options);
    update = applyToTag.bind(null, style, options);

    remove = function remove() {
      removeStyleElement(style);
    };
  }

  update(obj);
  return function updateStyle(newObj) {
    if (newObj) {
      if (newObj.css === obj.css && newObj.media === obj.media && newObj.sourceMap === obj.sourceMap) {
        return;
      }

      update(obj = newObj);
    } else {
      remove();
    }
  };
}

module.exports = function (list, options) {
  options = options || {};
  options.attributes = typeof options.attributes === 'object' ? options.attributes : {}; // Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
  // tags it will allow on a page

  if (!options.singleton && typeof options.singleton !== 'boolean') {
    options.singleton = isOldIE();
  }

  var styles = listToStyles(list, options);
  addStylesToDom(styles, options);
  return function update(newList) {
    var mayRemove = [];

    for (var i = 0; i < styles.length; i++) {
      var item = styles[i];
      var domStyle = stylesInDom[item.id];

      if (domStyle) {
        domStyle.refs--;
        mayRemove.push(domStyle);
      }
    }

    if (newList) {
      var newStyles = listToStyles(newList, options);
      addStylesToDom(newStyles, options);
    }

    for (var _i = 0; _i < mayRemove.length; _i++) {
      var _domStyle = mayRemove[_i];

      if (_domStyle.refs === 0) {
        for (var j = 0; j < _domStyle.parts.length; j++) {
          _domStyle.parts[j]();
        }

        delete stylesInDom[_domStyle.id];
      }
    }
  };
};

/***/ }),

/***/ "./src/hemingway.js":
/*!**************************!*\
  !*** ./src/hemingway.js ***!
  \**************************/
/*! exports provided: format */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "format", function() { return format; });
/*
Inspired by Sam Williams work (@samwsoftware)
https://medium.com/@samwsoftware/creating-my-own-hemingway-app-712c56212b17
https://github.com/SamWSoftware/Projects/tree/master/hemingway
*/

let data = {
  paragraphs: 0,
  sentences: 0,
  words: 0,
  characters: 0,
  hardSentences: 0,
  veryHardSentences: 0,
  adverbs: 0,
  passiveVoice: 0,
  complex: 0
};

function format(text) {
  data = {
    paragraphs: 0,
    sentences: 0,
    words: 0,
    characters: 0,
    hardSentences: 0,
    veryHardSentences: 0,
    adverbs: 0,
    passiveVoice: 0,
    complex: 0
  };

  // remove empty paragraphs
  let paragraphs = text.split("\n").filter(p => p);
  let hardSentences = paragraphs.map(p => getDifficultSentences(p));
  let inP = hardSentences.map(para => `<p>${para}</p>`);
  data.paragraphs = paragraphs.length;

  let cleanText = text.replace(/\s/g, "");
  data.characters = cleanText.length;

  return { markup: inP.join(" "), stats: data };
}

function getDifficultSentences(p) {
  let sentences = getSentenceFromParagraph(p + " ");
  data.sentences += sentences.length;
  let hardOrNot = sentences.map(sent => {
    let cleanSentence = sent.replace(/[^a-z0-9. ]/gi, "") + ".";
    let words = cleanSentence.split(" ").length;
    let letters = cleanSentence.split(" ").join("").length;
    data.words += words;
    sent = getAdverbs(sent);
    sent = getComplex(sent);
    sent = getPassive(sent);
    sent = getQualifier(sent);
    let level = calculateLevel(letters, words, 1);
    if (words < 14) {
      return sent;
    } else if (level >= 10 && level < 14) {
      data.hardSentences += 1;
      return `<span class="hardSentence">${sent}</span>`;
    } else if (level >= 14) {
      data.veryHardSentences += 1;
      return `<span class="veryHardSentence">${sent}</span>`;
    } else {
      return sent;
    }
  });

  return hardOrNot.join(" ");
}

function getPassive(sent) {
  let originalWords = sent.split(" ");
  let words = sent
    .replace(/[^a-z0-9. ]/gi, "")
    .toLowerCase()
    .split(" ");
  let ed = words.filter(word => word.match(/ed$/));
  if (ed.length > 0) {
    ed.forEach(match => {
      originalWords = checkPrewords(words, originalWords, match);
    });
  }
  return originalWords.join(" ");
}

function checkPrewords(words, originalWords, match) {
  let preWords = ["is", "are", "was", "were", "be", "been", "being"];
  let index = words.indexOf(match);
  if (preWords.indexOf(words[index - 1]) >= 0) {
    data.passiveVoice += 1;
    originalWords[index - 1] =
      '<span class="passive">' + originalWords[index - 1];
    originalWords[index] = originalWords[index] + "</span>";
    let next = checkPrewords(
      words.slice(index + 1),
      originalWords.slice(index + 1),
      match
    );
    return [...originalWords.slice(0, index + 1), ...next];
  } else {
    return originalWords;
  }
}

function getSentenceFromParagraph(p) {
  let sentences = p
    .split(". ")
    .filter(s => s.length > 0)
    .map(s => s + ".");
  return sentences;
}

function calculateLevel(letters, words, sentences) {
  if (words === 0 || sentences === 0) {
    return 0;
  }
  let level = Math.round(
    4.71 * (letters / words) + 0.5 * words / sentences - 21.43
  );
  return level <= 0 ? 0 : level;
}

function getAdverbs(sentence) {
  let lyWords = getLyWords();
  return sentence
    .split(" ")
    .map(word => {
      if (
        word.replace(/[^a-z0-9. ]/gi, "").match(/ly$/) &&
        lyWords[word.replace(/[^a-z0-9. ]/gi, "").toLowerCase()] === undefined
      ) {
        data.adverbs += 1;
        return `<span class="adverb">${word}</span>`;
      } else {
        return word;
      }
    })
    .join(" ");
}

function getComplex(sentence) {
  let words = getComplexWords();
  let wordList = Object.keys(words);
  wordList.forEach(key => {
    sentence = findAndSpan(sentence, key, "complex");
  });
  return sentence;
}

function findAndSpan(sentence, string, type) {
  let index = sentence.toLowerCase().indexOf(string);
  let a = { complex: "complex", qualifier: "adverbs" };
  if (index >= 0) {
    data[a[type]] += 1;
    sentence =
      sentence.slice(0, index) +
      `<span class="${type}">` +
      sentence.slice(index, index + string.length) +
      "</span>" +
      findAndSpan(sentence.slice(index + string.length), string, type);
  }
  return sentence;
}

function getQualifier(sentence) {
  let qualifiers = getQualifyingWords();
  let wordList = Object.keys(qualifiers);
  wordList.forEach(key => {
    sentence = findAndSpan(sentence, key, "qualifier");
  });
  return sentence;
}

function getQualifyingWords() {
  return {
    "i believe": 1,
    "i consider": 1,
    "i don't believe": 1,
    "i don't consider": 1,
    "i don't feel": 1,
    "i don't suggest": 1,
    "i don't think": 1,
    "i feel": 1,
    "i hope to": 1,
    "i might": 1,
    "i suggest": 1,
    "i think": 1,
    "i was wondering": 1,
    "i will try": 1,
    "i wonder": 1,
    "in my opinion": 1,
    "is kind of": 1,
    "is sort of": 1,
    just: 1,
    maybe: 1,
    perhaps: 1,
    possibly: 1,
    "we believe": 1,
    "we consider": 1,
    "we don't believe": 1,
    "we don't consider": 1,
    "we don't feel": 1,
    "we don't suggest": 1,
    "we don't think": 1,
    "we feel": 1,
    "we hope to": 1,
    "we might": 1,
    "we suggest": 1,
    "we think": 1,
    "we were wondering": 1,
    "we will try": 1,
    "we wonder": 1
  };
}

function getLyWords() {
  return {
    actually: 1,
    additionally: 1,
    allegedly: 1,
    ally: 1,
    alternatively: 1,
    anomaly: 1,
    apply: 1,
    approximately: 1,
    ashely: 1,
    ashly: 1,
    assembly: 1,
    awfully: 1,
    baily: 1,
    belly: 1,
    bely: 1,
    billy: 1,
    bradly: 1,
    bristly: 1,
    bubbly: 1,
    bully: 1,
    burly: 1,
    butterfly: 1,
    carly: 1,
    charly: 1,
    chilly: 1,
    comely: 1,
    completely: 1,
    comply: 1,
    consequently: 1,
    costly: 1,
    courtly: 1,
    crinkly: 1,
    crumbly: 1,
    cuddly: 1,
    curly: 1,
    currently: 1,
    daily: 1,
    dastardly: 1,
    deadly: 1,
    deathly: 1,
    definitely: 1,
    dilly: 1,
    disorderly: 1,
    doily: 1,
    dolly: 1,
    dragonfly: 1,
    early: 1,
    elderly: 1,
    elly: 1,
    emily: 1,
    especially: 1,
    exactly: 1,
    exclusively: 1,
    family: 1,
    finally: 1,
    firefly: 1,
    folly: 1,
    friendly: 1,
    frilly: 1,
    gadfly: 1,
    gangly: 1,
    generally: 1,
    ghastly: 1,
    giggly: 1,
    globally: 1,
    goodly: 1,
    gravelly: 1,
    grisly: 1,
    gully: 1,
    haily: 1,
    hally: 1,
    harly: 1,
    hardly: 1,
    heavenly: 1,
    hillbilly: 1,
    hilly: 1,
    holly: 1,
    holy: 1,
    homely: 1,
    homily: 1,
    horsefly: 1,
    hourly: 1,
    immediately: 1,
    instinctively: 1,
    imply: 1,
    italy: 1,
    jelly: 1,
    jiggly: 1,
    jilly: 1,
    jolly: 1,
    july: 1,
    karly: 1,
    kelly: 1,
    kindly: 1,
    lately: 1,
    likely: 1,
    lilly: 1,
    lily: 1,
    lively: 1,
    lolly: 1,
    lonely: 1,
    lovely: 1,
    lowly: 1,
    luckily: 1,
    mealy: 1,
    measly: 1,
    melancholy: 1,
    mentally: 1,
    molly: 1,
    monopoly: 1,
    monthly: 1,
    multiply: 1,
    nightly: 1,
    oily: 1,
    only: 1,
    orderly: 1,
    panoply: 1,
    particularly: 1,
    partly: 1,
    paully: 1,
    pearly: 1,
    pebbly: 1,
    polly: 1,
    potbelly: 1,
    presumably: 1,
    previously: 1,
    pualy: 1,
    quarterly: 1,
    rally: 1,
    rarely: 1,
    recently: 1,
    rely: 1,
    reply: 1,
    reportedly: 1,
    roughly: 1,
    sally: 1,
    scaly: 1,
    shapely: 1,
    shelly: 1,
    shirly: 1,
    shortly: 1,
    sickly: 1,
    silly: 1,
    sly: 1,
    smelly: 1,
    sparkly: 1,
    spindly: 1,
    spritely: 1,
    squiggly: 1,
    stately: 1,
    steely: 1,
    supply: 1,
    surly: 1,
    tally: 1,
    timely: 1,
    trolly: 1,
    ugly: 1,
    underbelly: 1,
    unfortunately: 1,
    unholy: 1,
    unlikely: 1,
    usually: 1,
    waverly: 1,
    weekly: 1,
    wholly: 1,
    willy: 1,
    wily: 1,
    wobbly: 1,
    wooly: 1,
    worldly: 1,
    wrinkly: 1,
    yearly: 1
  };
}

function getComplexWords() {
  return {
    "a number of": ["many", "some"],
    abundance: ["enough", "plenty"],
    "accede to": ["allow", "agree to"],
    accelerate: ["speed up"],
    accentuate: ["stress"],
    accompany: ["go with", "with"],
    accomplish: ["do"],
    accorded: ["given"],
    accrue: ["add", "gain"],
    acquiesce: ["agree"],
    acquire: ["get"],
    additional: ["more", "extra"],
    "adjacent to": ["next to"],
    adjustment: ["change"],
    admissible: ["allowed", "accepted"],
    advantageous: ["helpful"],
    "adversely impact": ["hurt"],
    advise: ["tell"],
    aforementioned: ["remove"],
    aggregate: ["total", "add"],
    aircraft: ["plane"],
    "all of": ["all"],
    alleviate: ["ease", "reduce"],
    allocate: ["divide"],
    "along the lines of": ["like", "as in"],
    "already existing": ["existing"],
    alternatively: ["or"],
    ameliorate: ["improve", "help"],
    anticipate: ["expect"],
    apparent: ["clear", "plain"],
    appreciable: ["many"],
    "as a means of": ["to"],
    "as of yet": ["yet"],
    "as to": ["on", "about"],
    "as yet": ["yet"],
    ascertain: ["find out", "learn"],
    assistance: ["help"],
    "at this time": ["now"],
    attain: ["meet"],
    "attributable to": ["because"],
    authorise: ["allow", "let"],
    authorize: ["allow", "let"],
    "because of the fact that": ["because"],
    belated: ["late"],
    "benefit from": ["enjoy"],
    bestow: ["give", "award"],
    "by virtue of": ["by", "under"],
    cease: ["stop"],
    "close proximity": ["near"],
    commence: ["begin or start"],
    "comply with": ["follow"],
    concerning: ["about", "on"],
    consequently: ["so"],
    consolidate: ["join", "merge"],
    constitutes: ["is", "forms", "makes up"],
    demonstrate: ["prove", "show"],
    depart: ["leave", "go"],
    designate: ["choose", "name"],
    discontinue: ["drop", "stop"],
    "due to the fact that": ["because", "since"],
    "each and every": ["each"],
    economical: ["cheap"],
    eliminate: ["cut", "drop", "end"],
    elucidate: ["explain"],
    employ: ["use"],
    endeavor: ["try"],
    enumerate: ["count"],
    equitable: ["fair"],
    equivalent: ["equal"],
    evaluate: ["test", "check"],
    evidenced: ["showed"],
    exclusively: ["only"],
    expedite: ["hurry"],
    expend: ["spend"],
    expiration: ["end"],
    facilitate: ["ease", "help"],
    "factual evidence": ["facts", "evidence"],
    feasible: ["workable"],
    finalise: ["complete", "finish"],
    finalize: ["complete", "finish"],
    "first and foremost": ["first"],
    "for the purpose of": ["to"],
    forfeit: ["lose", "give up"],
    formulate: ["plan"],
    "honest truth": ["truth"],
    however: ["but", "yet"],
    "if and when": ["if", "when"],
    impacted: ["affected", "harmed", "changed"],
    implement: ["install", "put in place", "tool"],
    "in a timely manner": ["on time"],
    "in accordance with": ["by", "under"],
    "in addition": ["also", "besides", "too"],
    "in all likelihood": ["probably"],
    "in an effort to": ["to"],
    "in between": ["between"],
    "in excess of": ["more than"],
    "in lieu of": ["instead"],
    "in light of the fact that": ["because"],
    "in many cases": ["often"],
    "in order to": ["to"],
    "in regard to": ["about", "concerning", "on"],
    "in some instances ": ["sometimes"],
    "in terms of": ["omit"],
    "in the near future": ["soon"],
    "in the process of": ["omit"],
    inception: ["start"],
    "incumbent upon": ["must"],
    indicate: ["say", "state", "or show"],
    indication: ["sign"],
    initiate: ["start"],
    "is applicable to": ["applies to"],
    "is authorised to": ["may"],
    "is authorized to": ["may"],
    "is responsible for": ["handles"],
    "it is essential": ["must", "need to"],
    literally: ["omit"],
    magnitude: ["size"],
    maximum: ["greatest", "largest", "most"],
    methodology: ["method"],
    minimise: ["cut"],
    minimize: ["cut"],
    minimum: ["least", "smallest", "small"],
    modify: ["change"],
    monitor: ["check", "watch", "track"],
    multiple: ["many"],
    necessitate: ["cause", "need"],
    nevertheless: ["still", "besides", "even so"],
    "not certain": ["uncertain"],
    "not many": ["few"],
    "not often": ["rarely"],
    "not unless": ["only if"],
    "not unlike": ["similar", "alike"],
    notwithstanding: ["in spite of", "still"],
    "null and void": ["use either null or void"],
    numerous: ["many"],
    objective: ["aim", "goal"],
    obligate: ["bind", "compel"],
    obtain: ["get"],
    "on the contrary": ["but", "so"],
    "on the other hand": ["omit", "but", "so"],
    "one particular": ["one"],
    optimum: ["best", "greatest", "most"],
    overall: ["omit"],
    "owing to the fact that": ["because", "since"],
    participate: ["take part"],
    particulars: ["details"],
    "pass away": ["die"],
    "pertaining to": ["about", "of", "on"],
    "point in time": ["time", "point", "moment", "now"],
    portion: ["part"],
    possess: ["have", "own"],
    preclude: ["prevent"],
    previously: ["before"],
    "prior to": ["before"],
    prioritise: ["rank", "focus on"],
    prioritize: ["rank", "focus on"],
    procure: ["buy", "get"],
    proficiency: ["skill"],
    "provided that": ["if"],
    purchase: ["buy", "sale"],
    "put simply": ["omit"],
    "readily apparent": ["clear"],
    "refer back": ["refer"],
    regarding: ["about", "of", "on"],
    relocate: ["move"],
    remainder: ["rest"],
    remuneration: ["payment"],
    require: ["must", "need"],
    requirement: ["need", "rule"],
    reside: ["live"],
    residence: ["house"],
    retain: ["keep"],
    satisfy: ["meet", "please"],
    shall: ["must", "will"],
    "should you wish": ["if you want"],
    "similar to": ["like"],
    solicit: ["ask for", "request"],
    "span across": ["span", "cross"],
    strategise: ["plan"],
    strategize: ["plan"],
    subsequent: ["later", "next", "after", "then"],
    substantial: ["large", "much"],
    "successfully complete": ["complete", "pass"],
    sufficient: ["enough"],
    terminate: ["end", "stop"],
    "the month of": ["omit"],
    therefore: ["thus", "so"],
    "this day and age": ["today"],
    "time period": ["time", "period"],
    "took advantage of": ["preyed on"],
    transmit: ["send"],
    transpire: ["happen"],
    "until such time as": ["until"],
    utilisation: ["use"],
    utilization: ["use"],
    utilise: ["use"],
    utilize: ["use"],
    validate: ["confirm"],
    "various different": ["various", "different"],
    "whether or not": ["whether"],
    "with respect to": ["on", "about"],
    "with the exception of": ["except for"],
    witnessed: ["saw", "seen"]
  };
}

function getJustifierWords() {
  return {
    "i believe": 1,
    "i consider": 1,
    "i don't believe": 1,
    "i don't consider": 1,
    "i don't feel": 1,
    "i don't suggest": 1,
    "i don't think": 1,
    "i feel": 1,
    "i hope to": 1,
    "i might": 1,
    "i suggest": 1,
    "i think": 1,
    "i was wondering": 1,
    "i will try": 1,
    "i wonder": 1,
    "in my opinion": 1,
    "is kind of": 1,
    "is sort of": 1,
    just: 1,
    maybe: 1,
    perhaps: 1,
    possibly: 1,
    "we believe": 1,
    "we consider": 1,
    "we don't believe": 1,
    "we don't consider": 1,
    "we don't feel": 1,
    "we don't suggest": 1,
    "we don't think": 1,
    "we feel": 1,
    "we hope to": 1,
    "we might": 1,
    "we suggest": 1,
    "we think": 1,
    "we were wondering": 1,
    "we will try": 1,
    "we wonder": 1
  };
}



/***/ }),

/***/ "./src/ui.css":
/*!********************!*\
  !*** ./src/ui.css ***!
  \********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var content = __webpack_require__(/*! !../node_modules/css-loader/dist/cjs.js!./ui.css */ "./node_modules/css-loader/dist/cjs.js!./src/ui.css");

if (typeof content === 'string') {
  content = [[module.i, content, '']];
}

var options = {}

options.insert = "head";
options.singleton = false;

var update = __webpack_require__(/*! ../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js")(content, options);

if (content.locals) {
  module.exports = content.locals;
}


/***/ }),

/***/ "./src/ui.ts":
/*!*******************!*\
  !*** ./src/ui.ts ***!
  \*******************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ui_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ui.css */ "./src/ui.css");
/* harmony import */ var _ui_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_ui_css__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _hemingway_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./hemingway.js */ "./src/hemingway.js");


function displayHtml(selector, value) {
    document.querySelector(selector).innerHTML = value;
}
function getReadabilityIndex(characters, words, sentences) {
    // Automated readability index
    // https://en.wikipedia.org/wiki/Automated_readability_index
    return Math.round(4.71 * (characters / words) +
        0.5 * (words / sentences) -
        21.43);
}
function getReadingLevel(readabilityIndex) {
    if (readabilityIndex <= 5) {
        return "Very easy";
    }
    else if (6 <= readabilityIndex && readabilityIndex < 8) {
        return "Easy";
    }
    else if (8 <= readabilityIndex && readabilityIndex < 12) {
        return "Standard";
    }
    else if (12 <= readabilityIndex && readabilityIndex < 14) {
        return "Difficult";
    }
    else if (14 <= readabilityIndex) {
        return "Very difficult";
    }
}
function displayData(data) {
    displayHtml("#readingLevel", `${getReadingLevel(getReadabilityIndex(data.characters, data.words, data.sentences))} reading level`);
    displayHtml("#counter", `${data.words} words, ${data.characters} characters`);
    displayHtml("#adverbCount", data.adverbs);
    displayHtml("#adverbLabel", `adverb${data.adverbs > 1 ? "s" : ""}, ${data.adverbs <= 2 ? "meeting goal" : "aim"} of 2 or fewer`);
    displayHtml("#passiveCount", data.passiveVoice);
    displayHtml("#passiveLabel", `use${data.passiveVoice > 1 ? "s" : ""} of passive voice, ${data.passiveVoice <= 2 ? "meeting goal" : "aim"} of 2 or fewer`);
    displayHtml("#complexCount", data.complex);
    displayHtml("#complexLabel", "phrases have a simpler alternatives");
    displayHtml("#hardSentenceCount", data.hardSentences);
    displayHtml("#veryHardSentenceCount", data.veryHardSentences);
}
onmessage = (event) => {
    const text = event.data.pluginMessage;
    const textOutput = document.getElementById('textOutput');
    // TODO process and highlight text and get Hemingway counts
    const analysis = Object(_hemingway_js__WEBPACK_IMPORTED_MODULE_1__["format"])(text);
    textOutput.innerHTML = analysis.markup;
    displayData(analysis.stats);
    // Get height to resize the modal
    const container = document.getElementById('hemingway');
    const resizeHeight = Math.min(610, container.getBoundingClientRect().height + 40);
    parent.postMessage({ pluginMessage: { type: 'resize', height: resizeHeight } }, '*');
};


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL3VpLmNzcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9kaXN0L3J1bnRpbWUvYXBpLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9zdHlsZS1sb2FkZXIvZGlzdC9ydW50aW1lL2luamVjdFN0eWxlc0ludG9TdHlsZVRhZy5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvaGVtaW5nd2F5LmpzIiwid2VicGFjazovLy8uL3NyYy91aS5jc3M/NmU5YSIsIndlYnBhY2s6Ly8vLi9zcmMvdWkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtRQUFBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBOzs7UUFHQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMENBQTBDLGdDQUFnQztRQUMxRTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLHdEQUF3RCxrQkFBa0I7UUFDMUU7UUFDQSxpREFBaUQsY0FBYztRQUMvRDs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0EseUNBQXlDLGlDQUFpQztRQUMxRSxnSEFBZ0gsbUJBQW1CLEVBQUU7UUFDckk7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwyQkFBMkIsMEJBQTBCLEVBQUU7UUFDdkQsaUNBQWlDLGVBQWU7UUFDaEQ7UUFDQTtRQUNBOztRQUVBO1FBQ0Esc0RBQXNELCtEQUErRDs7UUFFckg7UUFDQTs7O1FBR0E7UUFDQTs7Ozs7Ozs7Ozs7O0FDbEZBLDJCQUEyQixtQkFBTyxDQUFDLHFHQUFnRDtBQUNuRjtBQUNBLGNBQWMsUUFBUyxTQUFTLDBCQUEwQiwwQkFBMEIscUJBQXFCLEdBQUcsc0NBQXNDLGtCQUFrQixtQ0FBbUMsd0JBQXdCLHVCQUF1QixrQ0FBa0Msb0JBQW9CLEdBQUcsaUJBQWlCLHNCQUFzQixHQUFHLGNBQWMsbUJBQW1CLHNCQUFzQixHQUFHLGdDQUFnQyxrQkFBa0IsaUJBQWlCLHdCQUF3Qix1QkFBdUIsR0FBRyxtQkFBbUIsb0JBQW9CLEdBQUcsa0JBQWtCLHdCQUF3Qix1QkFBdUIsb0JBQW9CLGlCQUFpQix1QkFBdUIsR0FBRyxrQkFBa0IscUJBQXFCLEdBQUcsVUFBVSxpQkFBaUIsdUJBQXVCLEdBQUcsYUFBYSx3QkFBd0IsR0FBRyxnQkFBZ0Isd0JBQXdCLEdBQUcsY0FBYyx3QkFBd0IsR0FBRyxjQUFjLHdCQUF3QixHQUFHLG1CQUFtQix3QkFBd0IsR0FBRyx1QkFBdUIsd0JBQXdCLEdBQUcsaUJBQWlCLHNCQUFzQixxQkFBcUIseUJBQXlCLG9CQUFvQixHQUFHLE9BQU8sK0JBQStCLDhCQUE4QixvQkFBb0IsdUJBQXVCLG1CQUFtQixHQUFHOzs7Ozs7Ozs7Ozs7O0FDRjF2Qzs7QUFFYjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGdCQUFnQjs7QUFFaEI7QUFDQTtBQUNBOztBQUVBO0FBQ0EsMkNBQTJDLHFCQUFxQjtBQUNoRTs7QUFFQTtBQUNBLEtBQUs7QUFDTCxJQUFJO0FBQ0o7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUEsbUJBQW1CLGlCQUFpQjtBQUNwQztBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLG9CQUFvQixxQkFBcUI7QUFDekMsNkJBQTZCO0FBQzdCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0EsOEJBQThCOztBQUU5Qjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBLENBQUM7OztBQUdEO0FBQ0E7QUFDQTtBQUNBLHFEQUFxRCxjQUFjO0FBQ25FO0FBQ0EsQzs7Ozs7Ozs7Ozs7O0FDekZhOztBQUViOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQSx1REFBdUQ7O0FBRXZEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7O0FBRUEsaUJBQWlCLGlCQUFpQjtBQUNsQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUCxLQUFLO0FBQ0w7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQSxpQkFBaUIsbUJBQW1CO0FBQ3BDO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLFlBQVksMkJBQTJCO0FBQ3ZDO0FBQ0E7O0FBRUEsWUFBWSx1QkFBdUI7QUFDbkM7QUFDQTtBQUNBLEtBQUs7QUFDTDs7QUFFQSxZQUFZLHVCQUF1QjtBQUNuQztBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBLGdCQUFnQixLQUF3QyxHQUFHLHNCQUFpQixHQUFHLFNBQUk7O0FBRW5GO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBLGtDQUFrQzs7QUFFbEM7O0FBRUE7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLHlEQUF5RDtBQUN6RCxHQUFHOztBQUVIOzs7QUFHQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSx3RkFBd0Y7QUFDeEY7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLG1CQUFtQixtQkFBbUI7QUFDdEM7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLG9CQUFvQix1QkFBdUI7QUFDM0M7O0FBRUE7QUFDQSx1QkFBdUIsNEJBQTRCO0FBQ25EO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFOzs7Ozs7Ozs7Ozs7QUN6UkE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsNENBQTRDLEtBQUs7QUFDakQ7O0FBRUE7QUFDQTs7QUFFQSxVQUFVO0FBQ1Y7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsMkNBQTJDLEtBQUs7QUFDaEQsS0FBSztBQUNMO0FBQ0EsK0NBQStDLEtBQUs7QUFDcEQsS0FBSztBQUNMO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHVDQUF1QyxLQUFLO0FBQzVDLE9BQU87QUFDUDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBOztBQUVBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxzQkFBc0IsS0FBSztBQUMzQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7OztBQ2xvQkEsY0FBYyxtQkFBTyxDQUFDLDRHQUFtRDs7QUFFekU7QUFDQSxjQUFjLFFBQVM7QUFDdkI7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQSxhQUFhLG1CQUFPLENBQUMsbUpBQXdFOztBQUU3RjtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7QUNmQTtBQUFBO0FBQUE7QUFBQTtBQUFrQjtBQUNzQjtBQUN4QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG9DQUFvQyxrRkFBa0Y7QUFDdEgsK0JBQStCLFdBQVcsVUFBVSxnQkFBZ0I7QUFDcEU7QUFDQSx5Q0FBeUMsNEJBQTRCLElBQUksMkNBQTJDO0FBQ3BIO0FBQ0EsdUNBQXVDLGlDQUFpQyxxQkFBcUIsZ0RBQWdEO0FBQzdJO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQiw0REFBTTtBQUMzQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esd0JBQXdCLGlCQUFpQix1Q0FBdUMsRUFBRTtBQUNsRiIsImZpbGUiOiJ1aS5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBcIi4vc3JjL3VpLnRzXCIpO1xuIiwiZXhwb3J0cyA9IG1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIi4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2Rpc3QvcnVudGltZS9hcGkuanNcIikoZmFsc2UpO1xuLy8gTW9kdWxlXG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCJib2R5IHtcXG4gIGZvbnQ6IDEycHggc2Fucy1zZXJpZjtcXG4gIG1hcmdpbjogOHB4IDIwcHggMjBweDtcXG4gIG92ZXJmbG93OiBoaWRkZW47XFxufVxcblxcbi8qIEZvciBzdGF0cyAqL1xcbi5jaGFyYWN0ZXJTdGF0cyB7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIHBhZGRpbmc6IDEycHggMjRweDtcXG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCAjZTNlM2UzO1xcbiAgbWFyZ2luOiAwIC0yMHB4O1xcbn1cXG4jcmVhZGluZ0xldmVsIHtcXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xcbn1cXG5cXG4jY291bnRlciB7XFxuICBjb2xvcjogIzgyODI4MjtcXG4gIHRleHQtYWxpZ246IHJpZ2h0O1xcbn1cXG5cXG4vKiBGb3IgSGVtaW5nd2F5ICovXFxuLnN0YXQge1xcbiAgZGlzcGxheTogZmxleDtcXG4gIHBhZGRpbmc6IDhweDtcXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICBib3JkZXItcmFkaXVzOiA0cHg7XFxufVxcblxcbi5zdGF0ICsgLnN0YXQge1xcbiAgbWFyZ2luLXRvcDogOHB4O1xcbn1cXG5cXG4uc3RhdCAuY291bnQge1xcbiAgYmFja2dyb3VuZDogI0ZGRkZGRjtcXG4gIGJvcmRlci1yYWRpdXM6IDRweDtcXG4gIG1pbi13aWR0aDogMjBweDtcXG4gIHBhZGRpbmc6IDRweDtcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG59XFxuXFxuLnN0YXQgLmxhYmVsIHtcXG4gIG1hcmdpbi1sZWZ0OiA4cHg7XFxufVxcblxcbnNwYW4ge1xcbiAgcGFkZGluZzogMnB4O1xcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xcbn1cXG5cXG4uYWR2ZXJiIHtcXG4gIGJhY2tncm91bmQ6ICNjNGUzZjM7XFxufVxcblxcbi5xdWFsaWZpZXIge1xcbiAgYmFja2dyb3VuZDogI2M0ZTNmMztcXG59XFxuXFxuLnBhc3NpdmUge1xcbiAgYmFja2dyb3VuZDogI2M0ZWQ5ZDtcXG59XFxuXFxuLmNvbXBsZXgge1xcbiAgYmFja2dyb3VuZDogI2UzYjdlODtcXG59XFxuXFxuLmhhcmRTZW50ZW5jZSB7XFxuICBiYWNrZ3JvdW5kOiAjZjdlY2I1O1xcbn1cXG5cXG4udmVyeUhhcmRTZW50ZW5jZSB7XFxuICBiYWNrZ3JvdW5kOiAjZTRiOWI5O1xcbn1cXG5cXG4jdGV4dE91dHB1dCB7XFxuICBtYXgtaGVpZ2h0OiAzMDBweDtcXG4gIG92ZXJmbG93OiBzY3JvbGw7XFxuICBtYXJnaW46IDAgLTIwcHggMjBweDtcXG4gIHBhZGRpbmc6IDAgMjBweDtcXG59XFxuXFxucCB7XFxuICAtd2Via2l0LW1hcmdpbi1iZWZvcmU6IDhweDtcXG4gIC13ZWJraXQtbWFyZ2luLWFmdGVyOiA4cHg7XFxuICBmb250LXNpemU6IDE2cHg7XFxuICBsaW5lLWhlaWdodDogMS40ZW07XFxuICBjb2xvcjogIzMzMzMzMztcXG59XCIsIFwiXCJdKTtcbiIsIlwidXNlIHN0cmljdFwiO1xuXG4vKlxuICBNSVQgTGljZW5zZSBodHRwOi8vd3d3Lm9wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL21pdC1saWNlbnNlLnBocFxuICBBdXRob3IgVG9iaWFzIEtvcHBlcnMgQHNva3JhXG4qL1xuLy8gY3NzIGJhc2UgY29kZSwgaW5qZWN0ZWQgYnkgdGhlIGNzcy1sb2FkZXJcbi8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBmdW5jLW5hbWVzXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uICh1c2VTb3VyY2VNYXApIHtcbiAgdmFyIGxpc3QgPSBbXTsgLy8gcmV0dXJuIHRoZSBsaXN0IG9mIG1vZHVsZXMgYXMgY3NzIHN0cmluZ1xuXG4gIGxpc3QudG9TdHJpbmcgPSBmdW5jdGlvbiB0b1N0cmluZygpIHtcbiAgICByZXR1cm4gdGhpcy5tYXAoZnVuY3Rpb24gKGl0ZW0pIHtcbiAgICAgIHZhciBjb250ZW50ID0gY3NzV2l0aE1hcHBpbmdUb1N0cmluZyhpdGVtLCB1c2VTb3VyY2VNYXApO1xuXG4gICAgICBpZiAoaXRlbVsyXSkge1xuICAgICAgICByZXR1cm4gXCJAbWVkaWEgXCIuY29uY2F0KGl0ZW1bMl0sIFwie1wiKS5jb25jYXQoY29udGVudCwgXCJ9XCIpO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gY29udGVudDtcbiAgICB9KS5qb2luKCcnKTtcbiAgfTsgLy8gaW1wb3J0IGEgbGlzdCBvZiBtb2R1bGVzIGludG8gdGhlIGxpc3RcbiAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIGZ1bmMtbmFtZXNcblxuXG4gIGxpc3QuaSA9IGZ1bmN0aW9uIChtb2R1bGVzLCBtZWRpYVF1ZXJ5KSB7XG4gICAgaWYgKHR5cGVvZiBtb2R1bGVzID09PSAnc3RyaW5nJykge1xuICAgICAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIG5vLXBhcmFtLXJlYXNzaWduXG4gICAgICBtb2R1bGVzID0gW1tudWxsLCBtb2R1bGVzLCAnJ11dO1xuICAgIH1cblxuICAgIHZhciBhbHJlYWR5SW1wb3J0ZWRNb2R1bGVzID0ge307XG5cbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IHRoaXMubGVuZ3RoOyBpKyspIHtcbiAgICAgIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBwcmVmZXItZGVzdHJ1Y3R1cmluZ1xuICAgICAgdmFyIGlkID0gdGhpc1tpXVswXTtcblxuICAgICAgaWYgKGlkICE9IG51bGwpIHtcbiAgICAgICAgYWxyZWFkeUltcG9ydGVkTW9kdWxlc1tpZF0gPSB0cnVlO1xuICAgICAgfVxuICAgIH1cblxuICAgIGZvciAodmFyIF9pID0gMDsgX2kgPCBtb2R1bGVzLmxlbmd0aDsgX2krKykge1xuICAgICAgdmFyIGl0ZW0gPSBtb2R1bGVzW19pXTsgLy8gc2tpcCBhbHJlYWR5IGltcG9ydGVkIG1vZHVsZVxuICAgICAgLy8gdGhpcyBpbXBsZW1lbnRhdGlvbiBpcyBub3QgMTAwJSBwZXJmZWN0IGZvciB3ZWlyZCBtZWRpYSBxdWVyeSBjb21iaW5hdGlvbnNcbiAgICAgIC8vIHdoZW4gYSBtb2R1bGUgaXMgaW1wb3J0ZWQgbXVsdGlwbGUgdGltZXMgd2l0aCBkaWZmZXJlbnQgbWVkaWEgcXVlcmllcy5cbiAgICAgIC8vIEkgaG9wZSB0aGlzIHdpbGwgbmV2ZXIgb2NjdXIgKEhleSB0aGlzIHdheSB3ZSBoYXZlIHNtYWxsZXIgYnVuZGxlcylcblxuICAgICAgaWYgKGl0ZW1bMF0gPT0gbnVsbCB8fCAhYWxyZWFkeUltcG9ydGVkTW9kdWxlc1tpdGVtWzBdXSkge1xuICAgICAgICBpZiAobWVkaWFRdWVyeSAmJiAhaXRlbVsyXSkge1xuICAgICAgICAgIGl0ZW1bMl0gPSBtZWRpYVF1ZXJ5O1xuICAgICAgICB9IGVsc2UgaWYgKG1lZGlhUXVlcnkpIHtcbiAgICAgICAgICBpdGVtWzJdID0gXCIoXCIuY29uY2F0KGl0ZW1bMl0sIFwiKSBhbmQgKFwiKS5jb25jYXQobWVkaWFRdWVyeSwgXCIpXCIpO1xuICAgICAgICB9XG5cbiAgICAgICAgbGlzdC5wdXNoKGl0ZW0pO1xuICAgICAgfVxuICAgIH1cbiAgfTtcblxuICByZXR1cm4gbGlzdDtcbn07XG5cbmZ1bmN0aW9uIGNzc1dpdGhNYXBwaW5nVG9TdHJpbmcoaXRlbSwgdXNlU291cmNlTWFwKSB7XG4gIHZhciBjb250ZW50ID0gaXRlbVsxXSB8fCAnJzsgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIHByZWZlci1kZXN0cnVjdHVyaW5nXG5cbiAgdmFyIGNzc01hcHBpbmcgPSBpdGVtWzNdO1xuXG4gIGlmICghY3NzTWFwcGluZykge1xuICAgIHJldHVybiBjb250ZW50O1xuICB9XG5cbiAgaWYgKHVzZVNvdXJjZU1hcCAmJiB0eXBlb2YgYnRvYSA9PT0gJ2Z1bmN0aW9uJykge1xuICAgIHZhciBzb3VyY2VNYXBwaW5nID0gdG9Db21tZW50KGNzc01hcHBpbmcpO1xuICAgIHZhciBzb3VyY2VVUkxzID0gY3NzTWFwcGluZy5zb3VyY2VzLm1hcChmdW5jdGlvbiAoc291cmNlKSB7XG4gICAgICByZXR1cm4gXCIvKiMgc291cmNlVVJMPVwiLmNvbmNhdChjc3NNYXBwaW5nLnNvdXJjZVJvb3QpLmNvbmNhdChzb3VyY2UsIFwiICovXCIpO1xuICAgIH0pO1xuICAgIHJldHVybiBbY29udGVudF0uY29uY2F0KHNvdXJjZVVSTHMpLmNvbmNhdChbc291cmNlTWFwcGluZ10pLmpvaW4oJ1xcbicpO1xuICB9XG5cbiAgcmV0dXJuIFtjb250ZW50XS5qb2luKCdcXG4nKTtcbn0gLy8gQWRhcHRlZCBmcm9tIGNvbnZlcnQtc291cmNlLW1hcCAoTUlUKVxuXG5cbmZ1bmN0aW9uIHRvQ29tbWVudChzb3VyY2VNYXApIHtcbiAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIG5vLXVuZGVmXG4gIHZhciBiYXNlNjQgPSBidG9hKHVuZXNjYXBlKGVuY29kZVVSSUNvbXBvbmVudChKU09OLnN0cmluZ2lmeShzb3VyY2VNYXApKSkpO1xuICB2YXIgZGF0YSA9IFwic291cmNlTWFwcGluZ1VSTD1kYXRhOmFwcGxpY2F0aW9uL2pzb247Y2hhcnNldD11dGYtODtiYXNlNjQsXCIuY29uY2F0KGJhc2U2NCk7XG4gIHJldHVybiBcIi8qIyBcIi5jb25jYXQoZGF0YSwgXCIgKi9cIik7XG59IiwiXCJ1c2Ugc3RyaWN0XCI7XG5cbnZhciBzdHlsZXNJbkRvbSA9IHt9O1xuXG52YXIgaXNPbGRJRSA9IGZ1bmN0aW9uIGlzT2xkSUUoKSB7XG4gIHZhciBtZW1vO1xuICByZXR1cm4gZnVuY3Rpb24gbWVtb3JpemUoKSB7XG4gICAgaWYgKHR5cGVvZiBtZW1vID09PSAndW5kZWZpbmVkJykge1xuICAgICAgLy8gVGVzdCBmb3IgSUUgPD0gOSBhcyBwcm9wb3NlZCBieSBCcm93c2VyaGFja3NcbiAgICAgIC8vIEBzZWUgaHR0cDovL2Jyb3dzZXJoYWNrcy5jb20vI2hhY2stZTcxZDg2OTJmNjUzMzQxNzNmZWU3MTVjMjIyY2I4MDVcbiAgICAgIC8vIFRlc3RzIGZvciBleGlzdGVuY2Ugb2Ygc3RhbmRhcmQgZ2xvYmFscyBpcyB0byBhbGxvdyBzdHlsZS1sb2FkZXJcbiAgICAgIC8vIHRvIG9wZXJhdGUgY29ycmVjdGx5IGludG8gbm9uLXN0YW5kYXJkIGVudmlyb25tZW50c1xuICAgICAgLy8gQHNlZSBodHRwczovL2dpdGh1Yi5jb20vd2VicGFjay1jb250cmliL3N0eWxlLWxvYWRlci9pc3N1ZXMvMTc3XG4gICAgICBtZW1vID0gQm9vbGVhbih3aW5kb3cgJiYgZG9jdW1lbnQgJiYgZG9jdW1lbnQuYWxsICYmICF3aW5kb3cuYXRvYik7XG4gICAgfVxuXG4gICAgcmV0dXJuIG1lbW87XG4gIH07XG59KCk7XG5cbnZhciBnZXRUYXJnZXQgPSBmdW5jdGlvbiBnZXRUYXJnZXQoKSB7XG4gIHZhciBtZW1vID0ge307XG4gIHJldHVybiBmdW5jdGlvbiBtZW1vcml6ZSh0YXJnZXQpIHtcbiAgICBpZiAodHlwZW9mIG1lbW9bdGFyZ2V0XSA9PT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgIHZhciBzdHlsZVRhcmdldCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IodGFyZ2V0KTsgLy8gU3BlY2lhbCBjYXNlIHRvIHJldHVybiBoZWFkIG9mIGlmcmFtZSBpbnN0ZWFkIG9mIGlmcmFtZSBpdHNlbGZcblxuICAgICAgaWYgKHdpbmRvdy5IVE1MSUZyYW1lRWxlbWVudCAmJiBzdHlsZVRhcmdldCBpbnN0YW5jZW9mIHdpbmRvdy5IVE1MSUZyYW1lRWxlbWVudCkge1xuICAgICAgICB0cnkge1xuICAgICAgICAgIC8vIFRoaXMgd2lsbCB0aHJvdyBhbiBleGNlcHRpb24gaWYgYWNjZXNzIHRvIGlmcmFtZSBpcyBibG9ja2VkXG4gICAgICAgICAgLy8gZHVlIHRvIGNyb3NzLW9yaWdpbiByZXN0cmljdGlvbnNcbiAgICAgICAgICBzdHlsZVRhcmdldCA9IHN0eWxlVGFyZ2V0LmNvbnRlbnREb2N1bWVudC5oZWFkO1xuICAgICAgICB9IGNhdGNoIChlKSB7XG4gICAgICAgICAgLy8gaXN0YW5idWwgaWdub3JlIG5leHRcbiAgICAgICAgICBzdHlsZVRhcmdldCA9IG51bGw7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgbWVtb1t0YXJnZXRdID0gc3R5bGVUYXJnZXQ7XG4gICAgfVxuXG4gICAgcmV0dXJuIG1lbW9bdGFyZ2V0XTtcbiAgfTtcbn0oKTtcblxuZnVuY3Rpb24gbGlzdFRvU3R5bGVzKGxpc3QsIG9wdGlvbnMpIHtcbiAgdmFyIHN0eWxlcyA9IFtdO1xuICB2YXIgbmV3U3R5bGVzID0ge307XG5cbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBsaXN0Lmxlbmd0aDsgaSsrKSB7XG4gICAgdmFyIGl0ZW0gPSBsaXN0W2ldO1xuICAgIHZhciBpZCA9IG9wdGlvbnMuYmFzZSA/IGl0ZW1bMF0gKyBvcHRpb25zLmJhc2UgOiBpdGVtWzBdO1xuICAgIHZhciBjc3MgPSBpdGVtWzFdO1xuICAgIHZhciBtZWRpYSA9IGl0ZW1bMl07XG4gICAgdmFyIHNvdXJjZU1hcCA9IGl0ZW1bM107XG4gICAgdmFyIHBhcnQgPSB7XG4gICAgICBjc3M6IGNzcyxcbiAgICAgIG1lZGlhOiBtZWRpYSxcbiAgICAgIHNvdXJjZU1hcDogc291cmNlTWFwXG4gICAgfTtcblxuICAgIGlmICghbmV3U3R5bGVzW2lkXSkge1xuICAgICAgc3R5bGVzLnB1c2gobmV3U3R5bGVzW2lkXSA9IHtcbiAgICAgICAgaWQ6IGlkLFxuICAgICAgICBwYXJ0czogW3BhcnRdXG4gICAgICB9KTtcbiAgICB9IGVsc2Uge1xuICAgICAgbmV3U3R5bGVzW2lkXS5wYXJ0cy5wdXNoKHBhcnQpO1xuICAgIH1cbiAgfVxuXG4gIHJldHVybiBzdHlsZXM7XG59XG5cbmZ1bmN0aW9uIGFkZFN0eWxlc1RvRG9tKHN0eWxlcywgb3B0aW9ucykge1xuICBmb3IgKHZhciBpID0gMDsgaSA8IHN0eWxlcy5sZW5ndGg7IGkrKykge1xuICAgIHZhciBpdGVtID0gc3R5bGVzW2ldO1xuICAgIHZhciBkb21TdHlsZSA9IHN0eWxlc0luRG9tW2l0ZW0uaWRdO1xuICAgIHZhciBqID0gMDtcblxuICAgIGlmIChkb21TdHlsZSkge1xuICAgICAgZG9tU3R5bGUucmVmcysrO1xuXG4gICAgICBmb3IgKDsgaiA8IGRvbVN0eWxlLnBhcnRzLmxlbmd0aDsgaisrKSB7XG4gICAgICAgIGRvbVN0eWxlLnBhcnRzW2pdKGl0ZW0ucGFydHNbal0pO1xuICAgICAgfVxuXG4gICAgICBmb3IgKDsgaiA8IGl0ZW0ucGFydHMubGVuZ3RoOyBqKyspIHtcbiAgICAgICAgZG9tU3R5bGUucGFydHMucHVzaChhZGRTdHlsZShpdGVtLnBhcnRzW2pdLCBvcHRpb25zKSk7XG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIHZhciBwYXJ0cyA9IFtdO1xuXG4gICAgICBmb3IgKDsgaiA8IGl0ZW0ucGFydHMubGVuZ3RoOyBqKyspIHtcbiAgICAgICAgcGFydHMucHVzaChhZGRTdHlsZShpdGVtLnBhcnRzW2pdLCBvcHRpb25zKSk7XG4gICAgICB9XG5cbiAgICAgIHN0eWxlc0luRG9tW2l0ZW0uaWRdID0ge1xuICAgICAgICBpZDogaXRlbS5pZCxcbiAgICAgICAgcmVmczogMSxcbiAgICAgICAgcGFydHM6IHBhcnRzXG4gICAgICB9O1xuICAgIH1cbiAgfVxufVxuXG5mdW5jdGlvbiBpbnNlcnRTdHlsZUVsZW1lbnQob3B0aW9ucykge1xuICB2YXIgc3R5bGUgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdzdHlsZScpO1xuXG4gIGlmICh0eXBlb2Ygb3B0aW9ucy5hdHRyaWJ1dGVzLm5vbmNlID09PSAndW5kZWZpbmVkJykge1xuICAgIHZhciBub25jZSA9IHR5cGVvZiBfX3dlYnBhY2tfbm9uY2VfXyAhPT0gJ3VuZGVmaW5lZCcgPyBfX3dlYnBhY2tfbm9uY2VfXyA6IG51bGw7XG5cbiAgICBpZiAobm9uY2UpIHtcbiAgICAgIG9wdGlvbnMuYXR0cmlidXRlcy5ub25jZSA9IG5vbmNlO1xuICAgIH1cbiAgfVxuXG4gIE9iamVjdC5rZXlzKG9wdGlvbnMuYXR0cmlidXRlcykuZm9yRWFjaChmdW5jdGlvbiAoa2V5KSB7XG4gICAgc3R5bGUuc2V0QXR0cmlidXRlKGtleSwgb3B0aW9ucy5hdHRyaWJ1dGVzW2tleV0pO1xuICB9KTtcblxuICBpZiAodHlwZW9mIG9wdGlvbnMuaW5zZXJ0ID09PSAnZnVuY3Rpb24nKSB7XG4gICAgb3B0aW9ucy5pbnNlcnQoc3R5bGUpO1xuICB9IGVsc2Uge1xuICAgIHZhciB0YXJnZXQgPSBnZXRUYXJnZXQob3B0aW9ucy5pbnNlcnQgfHwgJ2hlYWQnKTtcblxuICAgIGlmICghdGFyZ2V0KSB7XG4gICAgICB0aHJvdyBuZXcgRXJyb3IoXCJDb3VsZG4ndCBmaW5kIGEgc3R5bGUgdGFyZ2V0LiBUaGlzIHByb2JhYmx5IG1lYW5zIHRoYXQgdGhlIHZhbHVlIGZvciB0aGUgJ2luc2VydCcgcGFyYW1ldGVyIGlzIGludmFsaWQuXCIpO1xuICAgIH1cblxuICAgIHRhcmdldC5hcHBlbmRDaGlsZChzdHlsZSk7XG4gIH1cblxuICByZXR1cm4gc3R5bGU7XG59XG5cbmZ1bmN0aW9uIHJlbW92ZVN0eWxlRWxlbWVudChzdHlsZSkge1xuICAvLyBpc3RhbmJ1bCBpZ25vcmUgaWZcbiAgaWYgKHN0eWxlLnBhcmVudE5vZGUgPT09IG51bGwpIHtcbiAgICByZXR1cm4gZmFsc2U7XG4gIH1cblxuICBzdHlsZS5wYXJlbnROb2RlLnJlbW92ZUNoaWxkKHN0eWxlKTtcbn1cbi8qIGlzdGFuYnVsIGlnbm9yZSBuZXh0ICAqL1xuXG5cbnZhciByZXBsYWNlVGV4dCA9IGZ1bmN0aW9uIHJlcGxhY2VUZXh0KCkge1xuICB2YXIgdGV4dFN0b3JlID0gW107XG4gIHJldHVybiBmdW5jdGlvbiByZXBsYWNlKGluZGV4LCByZXBsYWNlbWVudCkge1xuICAgIHRleHRTdG9yZVtpbmRleF0gPSByZXBsYWNlbWVudDtcbiAgICByZXR1cm4gdGV4dFN0b3JlLmZpbHRlcihCb29sZWFuKS5qb2luKCdcXG4nKTtcbiAgfTtcbn0oKTtcblxuZnVuY3Rpb24gYXBwbHlUb1NpbmdsZXRvblRhZyhzdHlsZSwgaW5kZXgsIHJlbW92ZSwgb2JqKSB7XG4gIHZhciBjc3MgPSByZW1vdmUgPyAnJyA6IG9iai5jc3M7IC8vIEZvciBvbGQgSUVcblxuICAvKiBpc3RhbmJ1bCBpZ25vcmUgaWYgICovXG5cbiAgaWYgKHN0eWxlLnN0eWxlU2hlZXQpIHtcbiAgICBzdHlsZS5zdHlsZVNoZWV0LmNzc1RleHQgPSByZXBsYWNlVGV4dChpbmRleCwgY3NzKTtcbiAgfSBlbHNlIHtcbiAgICB2YXIgY3NzTm9kZSA9IGRvY3VtZW50LmNyZWF0ZVRleHROb2RlKGNzcyk7XG4gICAgdmFyIGNoaWxkTm9kZXMgPSBzdHlsZS5jaGlsZE5vZGVzO1xuXG4gICAgaWYgKGNoaWxkTm9kZXNbaW5kZXhdKSB7XG4gICAgICBzdHlsZS5yZW1vdmVDaGlsZChjaGlsZE5vZGVzW2luZGV4XSk7XG4gICAgfVxuXG4gICAgaWYgKGNoaWxkTm9kZXMubGVuZ3RoKSB7XG4gICAgICBzdHlsZS5pbnNlcnRCZWZvcmUoY3NzTm9kZSwgY2hpbGROb2Rlc1tpbmRleF0pO1xuICAgIH0gZWxzZSB7XG4gICAgICBzdHlsZS5hcHBlbmRDaGlsZChjc3NOb2RlKTtcbiAgICB9XG4gIH1cbn1cblxuZnVuY3Rpb24gYXBwbHlUb1RhZyhzdHlsZSwgb3B0aW9ucywgb2JqKSB7XG4gIHZhciBjc3MgPSBvYmouY3NzO1xuICB2YXIgbWVkaWEgPSBvYmoubWVkaWE7XG4gIHZhciBzb3VyY2VNYXAgPSBvYmouc291cmNlTWFwO1xuXG4gIGlmIChtZWRpYSkge1xuICAgIHN0eWxlLnNldEF0dHJpYnV0ZSgnbWVkaWEnLCBtZWRpYSk7XG4gIH1cblxuICBpZiAoc291cmNlTWFwICYmIGJ0b2EpIHtcbiAgICBjc3MgKz0gXCJcXG4vKiMgc291cmNlTWFwcGluZ1VSTD1kYXRhOmFwcGxpY2F0aW9uL2pzb247YmFzZTY0LFwiLmNvbmNhdChidG9hKHVuZXNjYXBlKGVuY29kZVVSSUNvbXBvbmVudChKU09OLnN0cmluZ2lmeShzb3VyY2VNYXApKSkpLCBcIiAqL1wiKTtcbiAgfSAvLyBGb3Igb2xkIElFXG5cbiAgLyogaXN0YW5idWwgaWdub3JlIGlmICAqL1xuXG5cbiAgaWYgKHN0eWxlLnN0eWxlU2hlZXQpIHtcbiAgICBzdHlsZS5zdHlsZVNoZWV0LmNzc1RleHQgPSBjc3M7XG4gIH0gZWxzZSB7XG4gICAgd2hpbGUgKHN0eWxlLmZpcnN0Q2hpbGQpIHtcbiAgICAgIHN0eWxlLnJlbW92ZUNoaWxkKHN0eWxlLmZpcnN0Q2hpbGQpO1xuICAgIH1cblxuICAgIHN0eWxlLmFwcGVuZENoaWxkKGRvY3VtZW50LmNyZWF0ZVRleHROb2RlKGNzcykpO1xuICB9XG59XG5cbnZhciBzaW5nbGV0b24gPSBudWxsO1xudmFyIHNpbmdsZXRvbkNvdW50ZXIgPSAwO1xuXG5mdW5jdGlvbiBhZGRTdHlsZShvYmosIG9wdGlvbnMpIHtcbiAgdmFyIHN0eWxlO1xuICB2YXIgdXBkYXRlO1xuICB2YXIgcmVtb3ZlO1xuXG4gIGlmIChvcHRpb25zLnNpbmdsZXRvbikge1xuICAgIHZhciBzdHlsZUluZGV4ID0gc2luZ2xldG9uQ291bnRlcisrO1xuICAgIHN0eWxlID0gc2luZ2xldG9uIHx8IChzaW5nbGV0b24gPSBpbnNlcnRTdHlsZUVsZW1lbnQob3B0aW9ucykpO1xuICAgIHVwZGF0ZSA9IGFwcGx5VG9TaW5nbGV0b25UYWcuYmluZChudWxsLCBzdHlsZSwgc3R5bGVJbmRleCwgZmFsc2UpO1xuICAgIHJlbW92ZSA9IGFwcGx5VG9TaW5nbGV0b25UYWcuYmluZChudWxsLCBzdHlsZSwgc3R5bGVJbmRleCwgdHJ1ZSk7XG4gIH0gZWxzZSB7XG4gICAgc3R5bGUgPSBpbnNlcnRTdHlsZUVsZW1lbnQob3B0aW9ucyk7XG4gICAgdXBkYXRlID0gYXBwbHlUb1RhZy5iaW5kKG51bGwsIHN0eWxlLCBvcHRpb25zKTtcblxuICAgIHJlbW92ZSA9IGZ1bmN0aW9uIHJlbW92ZSgpIHtcbiAgICAgIHJlbW92ZVN0eWxlRWxlbWVudChzdHlsZSk7XG4gICAgfTtcbiAgfVxuXG4gIHVwZGF0ZShvYmopO1xuICByZXR1cm4gZnVuY3Rpb24gdXBkYXRlU3R5bGUobmV3T2JqKSB7XG4gICAgaWYgKG5ld09iaikge1xuICAgICAgaWYgKG5ld09iai5jc3MgPT09IG9iai5jc3MgJiYgbmV3T2JqLm1lZGlhID09PSBvYmoubWVkaWEgJiYgbmV3T2JqLnNvdXJjZU1hcCA9PT0gb2JqLnNvdXJjZU1hcCkge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIHVwZGF0ZShvYmogPSBuZXdPYmopO1xuICAgIH0gZWxzZSB7XG4gICAgICByZW1vdmUoKTtcbiAgICB9XG4gIH07XG59XG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGxpc3QsIG9wdGlvbnMpIHtcbiAgb3B0aW9ucyA9IG9wdGlvbnMgfHwge307XG4gIG9wdGlvbnMuYXR0cmlidXRlcyA9IHR5cGVvZiBvcHRpb25zLmF0dHJpYnV0ZXMgPT09ICdvYmplY3QnID8gb3B0aW9ucy5hdHRyaWJ1dGVzIDoge307IC8vIEZvcmNlIHNpbmdsZS10YWcgc29sdXRpb24gb24gSUU2LTksIHdoaWNoIGhhcyBhIGhhcmQgbGltaXQgb24gdGhlICMgb2YgPHN0eWxlPlxuICAvLyB0YWdzIGl0IHdpbGwgYWxsb3cgb24gYSBwYWdlXG5cbiAgaWYgKCFvcHRpb25zLnNpbmdsZXRvbiAmJiB0eXBlb2Ygb3B0aW9ucy5zaW5nbGV0b24gIT09ICdib29sZWFuJykge1xuICAgIG9wdGlvbnMuc2luZ2xldG9uID0gaXNPbGRJRSgpO1xuICB9XG5cbiAgdmFyIHN0eWxlcyA9IGxpc3RUb1N0eWxlcyhsaXN0LCBvcHRpb25zKTtcbiAgYWRkU3R5bGVzVG9Eb20oc3R5bGVzLCBvcHRpb25zKTtcbiAgcmV0dXJuIGZ1bmN0aW9uIHVwZGF0ZShuZXdMaXN0KSB7XG4gICAgdmFyIG1heVJlbW92ZSA9IFtdO1xuXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBzdHlsZXMubGVuZ3RoOyBpKyspIHtcbiAgICAgIHZhciBpdGVtID0gc3R5bGVzW2ldO1xuICAgICAgdmFyIGRvbVN0eWxlID0gc3R5bGVzSW5Eb21baXRlbS5pZF07XG5cbiAgICAgIGlmIChkb21TdHlsZSkge1xuICAgICAgICBkb21TdHlsZS5yZWZzLS07XG4gICAgICAgIG1heVJlbW92ZS5wdXNoKGRvbVN0eWxlKTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICBpZiAobmV3TGlzdCkge1xuICAgICAgdmFyIG5ld1N0eWxlcyA9IGxpc3RUb1N0eWxlcyhuZXdMaXN0LCBvcHRpb25zKTtcbiAgICAgIGFkZFN0eWxlc1RvRG9tKG5ld1N0eWxlcywgb3B0aW9ucyk7XG4gICAgfVxuXG4gICAgZm9yICh2YXIgX2kgPSAwOyBfaSA8IG1heVJlbW92ZS5sZW5ndGg7IF9pKyspIHtcbiAgICAgIHZhciBfZG9tU3R5bGUgPSBtYXlSZW1vdmVbX2ldO1xuXG4gICAgICBpZiAoX2RvbVN0eWxlLnJlZnMgPT09IDApIHtcbiAgICAgICAgZm9yICh2YXIgaiA9IDA7IGogPCBfZG9tU3R5bGUucGFydHMubGVuZ3RoOyBqKyspIHtcbiAgICAgICAgICBfZG9tU3R5bGUucGFydHNbal0oKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGRlbGV0ZSBzdHlsZXNJbkRvbVtfZG9tU3R5bGUuaWRdO1xuICAgICAgfVxuICAgIH1cbiAgfTtcbn07IiwiLypcbkluc3BpcmVkIGJ5IFNhbSBXaWxsaWFtcyB3b3JrIChAc2Ftd3NvZnR3YXJlKVxuaHR0cHM6Ly9tZWRpdW0uY29tL0BzYW13c29mdHdhcmUvY3JlYXRpbmctbXktb3duLWhlbWluZ3dheS1hcHAtNzEyYzU2MjEyYjE3XG5odHRwczovL2dpdGh1Yi5jb20vU2FtV1NvZnR3YXJlL1Byb2plY3RzL3RyZWUvbWFzdGVyL2hlbWluZ3dheVxuKi9cblxubGV0IGRhdGEgPSB7XG4gIHBhcmFncmFwaHM6IDAsXG4gIHNlbnRlbmNlczogMCxcbiAgd29yZHM6IDAsXG4gIGNoYXJhY3RlcnM6IDAsXG4gIGhhcmRTZW50ZW5jZXM6IDAsXG4gIHZlcnlIYXJkU2VudGVuY2VzOiAwLFxuICBhZHZlcmJzOiAwLFxuICBwYXNzaXZlVm9pY2U6IDAsXG4gIGNvbXBsZXg6IDBcbn07XG5cbmZ1bmN0aW9uIGZvcm1hdCh0ZXh0KSB7XG4gIGRhdGEgPSB7XG4gICAgcGFyYWdyYXBoczogMCxcbiAgICBzZW50ZW5jZXM6IDAsXG4gICAgd29yZHM6IDAsXG4gICAgY2hhcmFjdGVyczogMCxcbiAgICBoYXJkU2VudGVuY2VzOiAwLFxuICAgIHZlcnlIYXJkU2VudGVuY2VzOiAwLFxuICAgIGFkdmVyYnM6IDAsXG4gICAgcGFzc2l2ZVZvaWNlOiAwLFxuICAgIGNvbXBsZXg6IDBcbiAgfTtcblxuICAvLyByZW1vdmUgZW1wdHkgcGFyYWdyYXBoc1xuICBsZXQgcGFyYWdyYXBocyA9IHRleHQuc3BsaXQoXCJcXG5cIikuZmlsdGVyKHAgPT4gcCk7XG4gIGxldCBoYXJkU2VudGVuY2VzID0gcGFyYWdyYXBocy5tYXAocCA9PiBnZXREaWZmaWN1bHRTZW50ZW5jZXMocCkpO1xuICBsZXQgaW5QID0gaGFyZFNlbnRlbmNlcy5tYXAocGFyYSA9PiBgPHA+JHtwYXJhfTwvcD5gKTtcbiAgZGF0YS5wYXJhZ3JhcGhzID0gcGFyYWdyYXBocy5sZW5ndGg7XG5cbiAgbGV0IGNsZWFuVGV4dCA9IHRleHQucmVwbGFjZSgvXFxzL2csIFwiXCIpO1xuICBkYXRhLmNoYXJhY3RlcnMgPSBjbGVhblRleHQubGVuZ3RoO1xuXG4gIHJldHVybiB7IG1hcmt1cDogaW5QLmpvaW4oXCIgXCIpLCBzdGF0czogZGF0YSB9O1xufVxuXG5mdW5jdGlvbiBnZXREaWZmaWN1bHRTZW50ZW5jZXMocCkge1xuICBsZXQgc2VudGVuY2VzID0gZ2V0U2VudGVuY2VGcm9tUGFyYWdyYXBoKHAgKyBcIiBcIik7XG4gIGRhdGEuc2VudGVuY2VzICs9IHNlbnRlbmNlcy5sZW5ndGg7XG4gIGxldCBoYXJkT3JOb3QgPSBzZW50ZW5jZXMubWFwKHNlbnQgPT4ge1xuICAgIGxldCBjbGVhblNlbnRlbmNlID0gc2VudC5yZXBsYWNlKC9bXmEtejAtOS4gXS9naSwgXCJcIikgKyBcIi5cIjtcbiAgICBsZXQgd29yZHMgPSBjbGVhblNlbnRlbmNlLnNwbGl0KFwiIFwiKS5sZW5ndGg7XG4gICAgbGV0IGxldHRlcnMgPSBjbGVhblNlbnRlbmNlLnNwbGl0KFwiIFwiKS5qb2luKFwiXCIpLmxlbmd0aDtcbiAgICBkYXRhLndvcmRzICs9IHdvcmRzO1xuICAgIHNlbnQgPSBnZXRBZHZlcmJzKHNlbnQpO1xuICAgIHNlbnQgPSBnZXRDb21wbGV4KHNlbnQpO1xuICAgIHNlbnQgPSBnZXRQYXNzaXZlKHNlbnQpO1xuICAgIHNlbnQgPSBnZXRRdWFsaWZpZXIoc2VudCk7XG4gICAgbGV0IGxldmVsID0gY2FsY3VsYXRlTGV2ZWwobGV0dGVycywgd29yZHMsIDEpO1xuICAgIGlmICh3b3JkcyA8IDE0KSB7XG4gICAgICByZXR1cm4gc2VudDtcbiAgICB9IGVsc2UgaWYgKGxldmVsID49IDEwICYmIGxldmVsIDwgMTQpIHtcbiAgICAgIGRhdGEuaGFyZFNlbnRlbmNlcyArPSAxO1xuICAgICAgcmV0dXJuIGA8c3BhbiBjbGFzcz1cImhhcmRTZW50ZW5jZVwiPiR7c2VudH08L3NwYW4+YDtcbiAgICB9IGVsc2UgaWYgKGxldmVsID49IDE0KSB7XG4gICAgICBkYXRhLnZlcnlIYXJkU2VudGVuY2VzICs9IDE7XG4gICAgICByZXR1cm4gYDxzcGFuIGNsYXNzPVwidmVyeUhhcmRTZW50ZW5jZVwiPiR7c2VudH08L3NwYW4+YDtcbiAgICB9IGVsc2Uge1xuICAgICAgcmV0dXJuIHNlbnQ7XG4gICAgfVxuICB9KTtcblxuICByZXR1cm4gaGFyZE9yTm90LmpvaW4oXCIgXCIpO1xufVxuXG5mdW5jdGlvbiBnZXRQYXNzaXZlKHNlbnQpIHtcbiAgbGV0IG9yaWdpbmFsV29yZHMgPSBzZW50LnNwbGl0KFwiIFwiKTtcbiAgbGV0IHdvcmRzID0gc2VudFxuICAgIC5yZXBsYWNlKC9bXmEtejAtOS4gXS9naSwgXCJcIilcbiAgICAudG9Mb3dlckNhc2UoKVxuICAgIC5zcGxpdChcIiBcIik7XG4gIGxldCBlZCA9IHdvcmRzLmZpbHRlcih3b3JkID0+IHdvcmQubWF0Y2goL2VkJC8pKTtcbiAgaWYgKGVkLmxlbmd0aCA+IDApIHtcbiAgICBlZC5mb3JFYWNoKG1hdGNoID0+IHtcbiAgICAgIG9yaWdpbmFsV29yZHMgPSBjaGVja1ByZXdvcmRzKHdvcmRzLCBvcmlnaW5hbFdvcmRzLCBtYXRjaCk7XG4gICAgfSk7XG4gIH1cbiAgcmV0dXJuIG9yaWdpbmFsV29yZHMuam9pbihcIiBcIik7XG59XG5cbmZ1bmN0aW9uIGNoZWNrUHJld29yZHMod29yZHMsIG9yaWdpbmFsV29yZHMsIG1hdGNoKSB7XG4gIGxldCBwcmVXb3JkcyA9IFtcImlzXCIsIFwiYXJlXCIsIFwid2FzXCIsIFwid2VyZVwiLCBcImJlXCIsIFwiYmVlblwiLCBcImJlaW5nXCJdO1xuICBsZXQgaW5kZXggPSB3b3Jkcy5pbmRleE9mKG1hdGNoKTtcbiAgaWYgKHByZVdvcmRzLmluZGV4T2Yod29yZHNbaW5kZXggLSAxXSkgPj0gMCkge1xuICAgIGRhdGEucGFzc2l2ZVZvaWNlICs9IDE7XG4gICAgb3JpZ2luYWxXb3Jkc1tpbmRleCAtIDFdID1cbiAgICAgICc8c3BhbiBjbGFzcz1cInBhc3NpdmVcIj4nICsgb3JpZ2luYWxXb3Jkc1tpbmRleCAtIDFdO1xuICAgIG9yaWdpbmFsV29yZHNbaW5kZXhdID0gb3JpZ2luYWxXb3Jkc1tpbmRleF0gKyBcIjwvc3Bhbj5cIjtcbiAgICBsZXQgbmV4dCA9IGNoZWNrUHJld29yZHMoXG4gICAgICB3b3Jkcy5zbGljZShpbmRleCArIDEpLFxuICAgICAgb3JpZ2luYWxXb3Jkcy5zbGljZShpbmRleCArIDEpLFxuICAgICAgbWF0Y2hcbiAgICApO1xuICAgIHJldHVybiBbLi4ub3JpZ2luYWxXb3Jkcy5zbGljZSgwLCBpbmRleCArIDEpLCAuLi5uZXh0XTtcbiAgfSBlbHNlIHtcbiAgICByZXR1cm4gb3JpZ2luYWxXb3JkcztcbiAgfVxufVxuXG5mdW5jdGlvbiBnZXRTZW50ZW5jZUZyb21QYXJhZ3JhcGgocCkge1xuICBsZXQgc2VudGVuY2VzID0gcFxuICAgIC5zcGxpdChcIi4gXCIpXG4gICAgLmZpbHRlcihzID0+IHMubGVuZ3RoID4gMClcbiAgICAubWFwKHMgPT4gcyArIFwiLlwiKTtcbiAgcmV0dXJuIHNlbnRlbmNlcztcbn1cblxuZnVuY3Rpb24gY2FsY3VsYXRlTGV2ZWwobGV0dGVycywgd29yZHMsIHNlbnRlbmNlcykge1xuICBpZiAod29yZHMgPT09IDAgfHwgc2VudGVuY2VzID09PSAwKSB7XG4gICAgcmV0dXJuIDA7XG4gIH1cbiAgbGV0IGxldmVsID0gTWF0aC5yb3VuZChcbiAgICA0LjcxICogKGxldHRlcnMgLyB3b3JkcykgKyAwLjUgKiB3b3JkcyAvIHNlbnRlbmNlcyAtIDIxLjQzXG4gICk7XG4gIHJldHVybiBsZXZlbCA8PSAwID8gMCA6IGxldmVsO1xufVxuXG5mdW5jdGlvbiBnZXRBZHZlcmJzKHNlbnRlbmNlKSB7XG4gIGxldCBseVdvcmRzID0gZ2V0THlXb3JkcygpO1xuICByZXR1cm4gc2VudGVuY2VcbiAgICAuc3BsaXQoXCIgXCIpXG4gICAgLm1hcCh3b3JkID0+IHtcbiAgICAgIGlmIChcbiAgICAgICAgd29yZC5yZXBsYWNlKC9bXmEtejAtOS4gXS9naSwgXCJcIikubWF0Y2goL2x5JC8pICYmXG4gICAgICAgIGx5V29yZHNbd29yZC5yZXBsYWNlKC9bXmEtejAtOS4gXS9naSwgXCJcIikudG9Mb3dlckNhc2UoKV0gPT09IHVuZGVmaW5lZFxuICAgICAgKSB7XG4gICAgICAgIGRhdGEuYWR2ZXJicyArPSAxO1xuICAgICAgICByZXR1cm4gYDxzcGFuIGNsYXNzPVwiYWR2ZXJiXCI+JHt3b3JkfTwvc3Bhbj5gO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgcmV0dXJuIHdvcmQ7XG4gICAgICB9XG4gICAgfSlcbiAgICAuam9pbihcIiBcIik7XG59XG5cbmZ1bmN0aW9uIGdldENvbXBsZXgoc2VudGVuY2UpIHtcbiAgbGV0IHdvcmRzID0gZ2V0Q29tcGxleFdvcmRzKCk7XG4gIGxldCB3b3JkTGlzdCA9IE9iamVjdC5rZXlzKHdvcmRzKTtcbiAgd29yZExpc3QuZm9yRWFjaChrZXkgPT4ge1xuICAgIHNlbnRlbmNlID0gZmluZEFuZFNwYW4oc2VudGVuY2UsIGtleSwgXCJjb21wbGV4XCIpO1xuICB9KTtcbiAgcmV0dXJuIHNlbnRlbmNlO1xufVxuXG5mdW5jdGlvbiBmaW5kQW5kU3BhbihzZW50ZW5jZSwgc3RyaW5nLCB0eXBlKSB7XG4gIGxldCBpbmRleCA9IHNlbnRlbmNlLnRvTG93ZXJDYXNlKCkuaW5kZXhPZihzdHJpbmcpO1xuICBsZXQgYSA9IHsgY29tcGxleDogXCJjb21wbGV4XCIsIHF1YWxpZmllcjogXCJhZHZlcmJzXCIgfTtcbiAgaWYgKGluZGV4ID49IDApIHtcbiAgICBkYXRhW2FbdHlwZV1dICs9IDE7XG4gICAgc2VudGVuY2UgPVxuICAgICAgc2VudGVuY2Uuc2xpY2UoMCwgaW5kZXgpICtcbiAgICAgIGA8c3BhbiBjbGFzcz1cIiR7dHlwZX1cIj5gICtcbiAgICAgIHNlbnRlbmNlLnNsaWNlKGluZGV4LCBpbmRleCArIHN0cmluZy5sZW5ndGgpICtcbiAgICAgIFwiPC9zcGFuPlwiICtcbiAgICAgIGZpbmRBbmRTcGFuKHNlbnRlbmNlLnNsaWNlKGluZGV4ICsgc3RyaW5nLmxlbmd0aCksIHN0cmluZywgdHlwZSk7XG4gIH1cbiAgcmV0dXJuIHNlbnRlbmNlO1xufVxuXG5mdW5jdGlvbiBnZXRRdWFsaWZpZXIoc2VudGVuY2UpIHtcbiAgbGV0IHF1YWxpZmllcnMgPSBnZXRRdWFsaWZ5aW5nV29yZHMoKTtcbiAgbGV0IHdvcmRMaXN0ID0gT2JqZWN0LmtleXMocXVhbGlmaWVycyk7XG4gIHdvcmRMaXN0LmZvckVhY2goa2V5ID0+IHtcbiAgICBzZW50ZW5jZSA9IGZpbmRBbmRTcGFuKHNlbnRlbmNlLCBrZXksIFwicXVhbGlmaWVyXCIpO1xuICB9KTtcbiAgcmV0dXJuIHNlbnRlbmNlO1xufVxuXG5mdW5jdGlvbiBnZXRRdWFsaWZ5aW5nV29yZHMoKSB7XG4gIHJldHVybiB7XG4gICAgXCJpIGJlbGlldmVcIjogMSxcbiAgICBcImkgY29uc2lkZXJcIjogMSxcbiAgICBcImkgZG9uJ3QgYmVsaWV2ZVwiOiAxLFxuICAgIFwiaSBkb24ndCBjb25zaWRlclwiOiAxLFxuICAgIFwiaSBkb24ndCBmZWVsXCI6IDEsXG4gICAgXCJpIGRvbid0IHN1Z2dlc3RcIjogMSxcbiAgICBcImkgZG9uJ3QgdGhpbmtcIjogMSxcbiAgICBcImkgZmVlbFwiOiAxLFxuICAgIFwiaSBob3BlIHRvXCI6IDEsXG4gICAgXCJpIG1pZ2h0XCI6IDEsXG4gICAgXCJpIHN1Z2dlc3RcIjogMSxcbiAgICBcImkgdGhpbmtcIjogMSxcbiAgICBcImkgd2FzIHdvbmRlcmluZ1wiOiAxLFxuICAgIFwiaSB3aWxsIHRyeVwiOiAxLFxuICAgIFwiaSB3b25kZXJcIjogMSxcbiAgICBcImluIG15IG9waW5pb25cIjogMSxcbiAgICBcImlzIGtpbmQgb2ZcIjogMSxcbiAgICBcImlzIHNvcnQgb2ZcIjogMSxcbiAgICBqdXN0OiAxLFxuICAgIG1heWJlOiAxLFxuICAgIHBlcmhhcHM6IDEsXG4gICAgcG9zc2libHk6IDEsXG4gICAgXCJ3ZSBiZWxpZXZlXCI6IDEsXG4gICAgXCJ3ZSBjb25zaWRlclwiOiAxLFxuICAgIFwid2UgZG9uJ3QgYmVsaWV2ZVwiOiAxLFxuICAgIFwid2UgZG9uJ3QgY29uc2lkZXJcIjogMSxcbiAgICBcIndlIGRvbid0IGZlZWxcIjogMSxcbiAgICBcIndlIGRvbid0IHN1Z2dlc3RcIjogMSxcbiAgICBcIndlIGRvbid0IHRoaW5rXCI6IDEsXG4gICAgXCJ3ZSBmZWVsXCI6IDEsXG4gICAgXCJ3ZSBob3BlIHRvXCI6IDEsXG4gICAgXCJ3ZSBtaWdodFwiOiAxLFxuICAgIFwid2Ugc3VnZ2VzdFwiOiAxLFxuICAgIFwid2UgdGhpbmtcIjogMSxcbiAgICBcIndlIHdlcmUgd29uZGVyaW5nXCI6IDEsXG4gICAgXCJ3ZSB3aWxsIHRyeVwiOiAxLFxuICAgIFwid2Ugd29uZGVyXCI6IDFcbiAgfTtcbn1cblxuZnVuY3Rpb24gZ2V0THlXb3JkcygpIHtcbiAgcmV0dXJuIHtcbiAgICBhY3R1YWxseTogMSxcbiAgICBhZGRpdGlvbmFsbHk6IDEsXG4gICAgYWxsZWdlZGx5OiAxLFxuICAgIGFsbHk6IDEsXG4gICAgYWx0ZXJuYXRpdmVseTogMSxcbiAgICBhbm9tYWx5OiAxLFxuICAgIGFwcGx5OiAxLFxuICAgIGFwcHJveGltYXRlbHk6IDEsXG4gICAgYXNoZWx5OiAxLFxuICAgIGFzaGx5OiAxLFxuICAgIGFzc2VtYmx5OiAxLFxuICAgIGF3ZnVsbHk6IDEsXG4gICAgYmFpbHk6IDEsXG4gICAgYmVsbHk6IDEsXG4gICAgYmVseTogMSxcbiAgICBiaWxseTogMSxcbiAgICBicmFkbHk6IDEsXG4gICAgYnJpc3RseTogMSxcbiAgICBidWJibHk6IDEsXG4gICAgYnVsbHk6IDEsXG4gICAgYnVybHk6IDEsXG4gICAgYnV0dGVyZmx5OiAxLFxuICAgIGNhcmx5OiAxLFxuICAgIGNoYXJseTogMSxcbiAgICBjaGlsbHk6IDEsXG4gICAgY29tZWx5OiAxLFxuICAgIGNvbXBsZXRlbHk6IDEsXG4gICAgY29tcGx5OiAxLFxuICAgIGNvbnNlcXVlbnRseTogMSxcbiAgICBjb3N0bHk6IDEsXG4gICAgY291cnRseTogMSxcbiAgICBjcmlua2x5OiAxLFxuICAgIGNydW1ibHk6IDEsXG4gICAgY3VkZGx5OiAxLFxuICAgIGN1cmx5OiAxLFxuICAgIGN1cnJlbnRseTogMSxcbiAgICBkYWlseTogMSxcbiAgICBkYXN0YXJkbHk6IDEsXG4gICAgZGVhZGx5OiAxLFxuICAgIGRlYXRobHk6IDEsXG4gICAgZGVmaW5pdGVseTogMSxcbiAgICBkaWxseTogMSxcbiAgICBkaXNvcmRlcmx5OiAxLFxuICAgIGRvaWx5OiAxLFxuICAgIGRvbGx5OiAxLFxuICAgIGRyYWdvbmZseTogMSxcbiAgICBlYXJseTogMSxcbiAgICBlbGRlcmx5OiAxLFxuICAgIGVsbHk6IDEsXG4gICAgZW1pbHk6IDEsXG4gICAgZXNwZWNpYWxseTogMSxcbiAgICBleGFjdGx5OiAxLFxuICAgIGV4Y2x1c2l2ZWx5OiAxLFxuICAgIGZhbWlseTogMSxcbiAgICBmaW5hbGx5OiAxLFxuICAgIGZpcmVmbHk6IDEsXG4gICAgZm9sbHk6IDEsXG4gICAgZnJpZW5kbHk6IDEsXG4gICAgZnJpbGx5OiAxLFxuICAgIGdhZGZseTogMSxcbiAgICBnYW5nbHk6IDEsXG4gICAgZ2VuZXJhbGx5OiAxLFxuICAgIGdoYXN0bHk6IDEsXG4gICAgZ2lnZ2x5OiAxLFxuICAgIGdsb2JhbGx5OiAxLFxuICAgIGdvb2RseTogMSxcbiAgICBncmF2ZWxseTogMSxcbiAgICBncmlzbHk6IDEsXG4gICAgZ3VsbHk6IDEsXG4gICAgaGFpbHk6IDEsXG4gICAgaGFsbHk6IDEsXG4gICAgaGFybHk6IDEsXG4gICAgaGFyZGx5OiAxLFxuICAgIGhlYXZlbmx5OiAxLFxuICAgIGhpbGxiaWxseTogMSxcbiAgICBoaWxseTogMSxcbiAgICBob2xseTogMSxcbiAgICBob2x5OiAxLFxuICAgIGhvbWVseTogMSxcbiAgICBob21pbHk6IDEsXG4gICAgaG9yc2VmbHk6IDEsXG4gICAgaG91cmx5OiAxLFxuICAgIGltbWVkaWF0ZWx5OiAxLFxuICAgIGluc3RpbmN0aXZlbHk6IDEsXG4gICAgaW1wbHk6IDEsXG4gICAgaXRhbHk6IDEsXG4gICAgamVsbHk6IDEsXG4gICAgamlnZ2x5OiAxLFxuICAgIGppbGx5OiAxLFxuICAgIGpvbGx5OiAxLFxuICAgIGp1bHk6IDEsXG4gICAga2FybHk6IDEsXG4gICAga2VsbHk6IDEsXG4gICAga2luZGx5OiAxLFxuICAgIGxhdGVseTogMSxcbiAgICBsaWtlbHk6IDEsXG4gICAgbGlsbHk6IDEsXG4gICAgbGlseTogMSxcbiAgICBsaXZlbHk6IDEsXG4gICAgbG9sbHk6IDEsXG4gICAgbG9uZWx5OiAxLFxuICAgIGxvdmVseTogMSxcbiAgICBsb3dseTogMSxcbiAgICBsdWNraWx5OiAxLFxuICAgIG1lYWx5OiAxLFxuICAgIG1lYXNseTogMSxcbiAgICBtZWxhbmNob2x5OiAxLFxuICAgIG1lbnRhbGx5OiAxLFxuICAgIG1vbGx5OiAxLFxuICAgIG1vbm9wb2x5OiAxLFxuICAgIG1vbnRobHk6IDEsXG4gICAgbXVsdGlwbHk6IDEsXG4gICAgbmlnaHRseTogMSxcbiAgICBvaWx5OiAxLFxuICAgIG9ubHk6IDEsXG4gICAgb3JkZXJseTogMSxcbiAgICBwYW5vcGx5OiAxLFxuICAgIHBhcnRpY3VsYXJseTogMSxcbiAgICBwYXJ0bHk6IDEsXG4gICAgcGF1bGx5OiAxLFxuICAgIHBlYXJseTogMSxcbiAgICBwZWJibHk6IDEsXG4gICAgcG9sbHk6IDEsXG4gICAgcG90YmVsbHk6IDEsXG4gICAgcHJlc3VtYWJseTogMSxcbiAgICBwcmV2aW91c2x5OiAxLFxuICAgIHB1YWx5OiAxLFxuICAgIHF1YXJ0ZXJseTogMSxcbiAgICByYWxseTogMSxcbiAgICByYXJlbHk6IDEsXG4gICAgcmVjZW50bHk6IDEsXG4gICAgcmVseTogMSxcbiAgICByZXBseTogMSxcbiAgICByZXBvcnRlZGx5OiAxLFxuICAgIHJvdWdobHk6IDEsXG4gICAgc2FsbHk6IDEsXG4gICAgc2NhbHk6IDEsXG4gICAgc2hhcGVseTogMSxcbiAgICBzaGVsbHk6IDEsXG4gICAgc2hpcmx5OiAxLFxuICAgIHNob3J0bHk6IDEsXG4gICAgc2lja2x5OiAxLFxuICAgIHNpbGx5OiAxLFxuICAgIHNseTogMSxcbiAgICBzbWVsbHk6IDEsXG4gICAgc3BhcmtseTogMSxcbiAgICBzcGluZGx5OiAxLFxuICAgIHNwcml0ZWx5OiAxLFxuICAgIHNxdWlnZ2x5OiAxLFxuICAgIHN0YXRlbHk6IDEsXG4gICAgc3RlZWx5OiAxLFxuICAgIHN1cHBseTogMSxcbiAgICBzdXJseTogMSxcbiAgICB0YWxseTogMSxcbiAgICB0aW1lbHk6IDEsXG4gICAgdHJvbGx5OiAxLFxuICAgIHVnbHk6IDEsXG4gICAgdW5kZXJiZWxseTogMSxcbiAgICB1bmZvcnR1bmF0ZWx5OiAxLFxuICAgIHVuaG9seTogMSxcbiAgICB1bmxpa2VseTogMSxcbiAgICB1c3VhbGx5OiAxLFxuICAgIHdhdmVybHk6IDEsXG4gICAgd2Vla2x5OiAxLFxuICAgIHdob2xseTogMSxcbiAgICB3aWxseTogMSxcbiAgICB3aWx5OiAxLFxuICAgIHdvYmJseTogMSxcbiAgICB3b29seTogMSxcbiAgICB3b3JsZGx5OiAxLFxuICAgIHdyaW5rbHk6IDEsXG4gICAgeWVhcmx5OiAxXG4gIH07XG59XG5cbmZ1bmN0aW9uIGdldENvbXBsZXhXb3JkcygpIHtcbiAgcmV0dXJuIHtcbiAgICBcImEgbnVtYmVyIG9mXCI6IFtcIm1hbnlcIiwgXCJzb21lXCJdLFxuICAgIGFidW5kYW5jZTogW1wiZW5vdWdoXCIsIFwicGxlbnR5XCJdLFxuICAgIFwiYWNjZWRlIHRvXCI6IFtcImFsbG93XCIsIFwiYWdyZWUgdG9cIl0sXG4gICAgYWNjZWxlcmF0ZTogW1wic3BlZWQgdXBcIl0sXG4gICAgYWNjZW50dWF0ZTogW1wic3RyZXNzXCJdLFxuICAgIGFjY29tcGFueTogW1wiZ28gd2l0aFwiLCBcIndpdGhcIl0sXG4gICAgYWNjb21wbGlzaDogW1wiZG9cIl0sXG4gICAgYWNjb3JkZWQ6IFtcImdpdmVuXCJdLFxuICAgIGFjY3J1ZTogW1wiYWRkXCIsIFwiZ2FpblwiXSxcbiAgICBhY3F1aWVzY2U6IFtcImFncmVlXCJdLFxuICAgIGFjcXVpcmU6IFtcImdldFwiXSxcbiAgICBhZGRpdGlvbmFsOiBbXCJtb3JlXCIsIFwiZXh0cmFcIl0sXG4gICAgXCJhZGphY2VudCB0b1wiOiBbXCJuZXh0IHRvXCJdLFxuICAgIGFkanVzdG1lbnQ6IFtcImNoYW5nZVwiXSxcbiAgICBhZG1pc3NpYmxlOiBbXCJhbGxvd2VkXCIsIFwiYWNjZXB0ZWRcIl0sXG4gICAgYWR2YW50YWdlb3VzOiBbXCJoZWxwZnVsXCJdLFxuICAgIFwiYWR2ZXJzZWx5IGltcGFjdFwiOiBbXCJodXJ0XCJdLFxuICAgIGFkdmlzZTogW1widGVsbFwiXSxcbiAgICBhZm9yZW1lbnRpb25lZDogW1wicmVtb3ZlXCJdLFxuICAgIGFnZ3JlZ2F0ZTogW1widG90YWxcIiwgXCJhZGRcIl0sXG4gICAgYWlyY3JhZnQ6IFtcInBsYW5lXCJdLFxuICAgIFwiYWxsIG9mXCI6IFtcImFsbFwiXSxcbiAgICBhbGxldmlhdGU6IFtcImVhc2VcIiwgXCJyZWR1Y2VcIl0sXG4gICAgYWxsb2NhdGU6IFtcImRpdmlkZVwiXSxcbiAgICBcImFsb25nIHRoZSBsaW5lcyBvZlwiOiBbXCJsaWtlXCIsIFwiYXMgaW5cIl0sXG4gICAgXCJhbHJlYWR5IGV4aXN0aW5nXCI6IFtcImV4aXN0aW5nXCJdLFxuICAgIGFsdGVybmF0aXZlbHk6IFtcIm9yXCJdLFxuICAgIGFtZWxpb3JhdGU6IFtcImltcHJvdmVcIiwgXCJoZWxwXCJdLFxuICAgIGFudGljaXBhdGU6IFtcImV4cGVjdFwiXSxcbiAgICBhcHBhcmVudDogW1wiY2xlYXJcIiwgXCJwbGFpblwiXSxcbiAgICBhcHByZWNpYWJsZTogW1wibWFueVwiXSxcbiAgICBcImFzIGEgbWVhbnMgb2ZcIjogW1widG9cIl0sXG4gICAgXCJhcyBvZiB5ZXRcIjogW1wieWV0XCJdLFxuICAgIFwiYXMgdG9cIjogW1wib25cIiwgXCJhYm91dFwiXSxcbiAgICBcImFzIHlldFwiOiBbXCJ5ZXRcIl0sXG4gICAgYXNjZXJ0YWluOiBbXCJmaW5kIG91dFwiLCBcImxlYXJuXCJdLFxuICAgIGFzc2lzdGFuY2U6IFtcImhlbHBcIl0sXG4gICAgXCJhdCB0aGlzIHRpbWVcIjogW1wibm93XCJdLFxuICAgIGF0dGFpbjogW1wibWVldFwiXSxcbiAgICBcImF0dHJpYnV0YWJsZSB0b1wiOiBbXCJiZWNhdXNlXCJdLFxuICAgIGF1dGhvcmlzZTogW1wiYWxsb3dcIiwgXCJsZXRcIl0sXG4gICAgYXV0aG9yaXplOiBbXCJhbGxvd1wiLCBcImxldFwiXSxcbiAgICBcImJlY2F1c2Ugb2YgdGhlIGZhY3QgdGhhdFwiOiBbXCJiZWNhdXNlXCJdLFxuICAgIGJlbGF0ZWQ6IFtcImxhdGVcIl0sXG4gICAgXCJiZW5lZml0IGZyb21cIjogW1wiZW5qb3lcIl0sXG4gICAgYmVzdG93OiBbXCJnaXZlXCIsIFwiYXdhcmRcIl0sXG4gICAgXCJieSB2aXJ0dWUgb2ZcIjogW1wiYnlcIiwgXCJ1bmRlclwiXSxcbiAgICBjZWFzZTogW1wic3RvcFwiXSxcbiAgICBcImNsb3NlIHByb3hpbWl0eVwiOiBbXCJuZWFyXCJdLFxuICAgIGNvbW1lbmNlOiBbXCJiZWdpbiBvciBzdGFydFwiXSxcbiAgICBcImNvbXBseSB3aXRoXCI6IFtcImZvbGxvd1wiXSxcbiAgICBjb25jZXJuaW5nOiBbXCJhYm91dFwiLCBcIm9uXCJdLFxuICAgIGNvbnNlcXVlbnRseTogW1wic29cIl0sXG4gICAgY29uc29saWRhdGU6IFtcImpvaW5cIiwgXCJtZXJnZVwiXSxcbiAgICBjb25zdGl0dXRlczogW1wiaXNcIiwgXCJmb3Jtc1wiLCBcIm1ha2VzIHVwXCJdLFxuICAgIGRlbW9uc3RyYXRlOiBbXCJwcm92ZVwiLCBcInNob3dcIl0sXG4gICAgZGVwYXJ0OiBbXCJsZWF2ZVwiLCBcImdvXCJdLFxuICAgIGRlc2lnbmF0ZTogW1wiY2hvb3NlXCIsIFwibmFtZVwiXSxcbiAgICBkaXNjb250aW51ZTogW1wiZHJvcFwiLCBcInN0b3BcIl0sXG4gICAgXCJkdWUgdG8gdGhlIGZhY3QgdGhhdFwiOiBbXCJiZWNhdXNlXCIsIFwic2luY2VcIl0sXG4gICAgXCJlYWNoIGFuZCBldmVyeVwiOiBbXCJlYWNoXCJdLFxuICAgIGVjb25vbWljYWw6IFtcImNoZWFwXCJdLFxuICAgIGVsaW1pbmF0ZTogW1wiY3V0XCIsIFwiZHJvcFwiLCBcImVuZFwiXSxcbiAgICBlbHVjaWRhdGU6IFtcImV4cGxhaW5cIl0sXG4gICAgZW1wbG95OiBbXCJ1c2VcIl0sXG4gICAgZW5kZWF2b3I6IFtcInRyeVwiXSxcbiAgICBlbnVtZXJhdGU6IFtcImNvdW50XCJdLFxuICAgIGVxdWl0YWJsZTogW1wiZmFpclwiXSxcbiAgICBlcXVpdmFsZW50OiBbXCJlcXVhbFwiXSxcbiAgICBldmFsdWF0ZTogW1widGVzdFwiLCBcImNoZWNrXCJdLFxuICAgIGV2aWRlbmNlZDogW1wic2hvd2VkXCJdLFxuICAgIGV4Y2x1c2l2ZWx5OiBbXCJvbmx5XCJdLFxuICAgIGV4cGVkaXRlOiBbXCJodXJyeVwiXSxcbiAgICBleHBlbmQ6IFtcInNwZW5kXCJdLFxuICAgIGV4cGlyYXRpb246IFtcImVuZFwiXSxcbiAgICBmYWNpbGl0YXRlOiBbXCJlYXNlXCIsIFwiaGVscFwiXSxcbiAgICBcImZhY3R1YWwgZXZpZGVuY2VcIjogW1wiZmFjdHNcIiwgXCJldmlkZW5jZVwiXSxcbiAgICBmZWFzaWJsZTogW1wid29ya2FibGVcIl0sXG4gICAgZmluYWxpc2U6IFtcImNvbXBsZXRlXCIsIFwiZmluaXNoXCJdLFxuICAgIGZpbmFsaXplOiBbXCJjb21wbGV0ZVwiLCBcImZpbmlzaFwiXSxcbiAgICBcImZpcnN0IGFuZCBmb3JlbW9zdFwiOiBbXCJmaXJzdFwiXSxcbiAgICBcImZvciB0aGUgcHVycG9zZSBvZlwiOiBbXCJ0b1wiXSxcbiAgICBmb3JmZWl0OiBbXCJsb3NlXCIsIFwiZ2l2ZSB1cFwiXSxcbiAgICBmb3JtdWxhdGU6IFtcInBsYW5cIl0sXG4gICAgXCJob25lc3QgdHJ1dGhcIjogW1widHJ1dGhcIl0sXG4gICAgaG93ZXZlcjogW1wiYnV0XCIsIFwieWV0XCJdLFxuICAgIFwiaWYgYW5kIHdoZW5cIjogW1wiaWZcIiwgXCJ3aGVuXCJdLFxuICAgIGltcGFjdGVkOiBbXCJhZmZlY3RlZFwiLCBcImhhcm1lZFwiLCBcImNoYW5nZWRcIl0sXG4gICAgaW1wbGVtZW50OiBbXCJpbnN0YWxsXCIsIFwicHV0IGluIHBsYWNlXCIsIFwidG9vbFwiXSxcbiAgICBcImluIGEgdGltZWx5IG1hbm5lclwiOiBbXCJvbiB0aW1lXCJdLFxuICAgIFwiaW4gYWNjb3JkYW5jZSB3aXRoXCI6IFtcImJ5XCIsIFwidW5kZXJcIl0sXG4gICAgXCJpbiBhZGRpdGlvblwiOiBbXCJhbHNvXCIsIFwiYmVzaWRlc1wiLCBcInRvb1wiXSxcbiAgICBcImluIGFsbCBsaWtlbGlob29kXCI6IFtcInByb2JhYmx5XCJdLFxuICAgIFwiaW4gYW4gZWZmb3J0IHRvXCI6IFtcInRvXCJdLFxuICAgIFwiaW4gYmV0d2VlblwiOiBbXCJiZXR3ZWVuXCJdLFxuICAgIFwiaW4gZXhjZXNzIG9mXCI6IFtcIm1vcmUgdGhhblwiXSxcbiAgICBcImluIGxpZXUgb2ZcIjogW1wiaW5zdGVhZFwiXSxcbiAgICBcImluIGxpZ2h0IG9mIHRoZSBmYWN0IHRoYXRcIjogW1wiYmVjYXVzZVwiXSxcbiAgICBcImluIG1hbnkgY2FzZXNcIjogW1wib2Z0ZW5cIl0sXG4gICAgXCJpbiBvcmRlciB0b1wiOiBbXCJ0b1wiXSxcbiAgICBcImluIHJlZ2FyZCB0b1wiOiBbXCJhYm91dFwiLCBcImNvbmNlcm5pbmdcIiwgXCJvblwiXSxcbiAgICBcImluIHNvbWUgaW5zdGFuY2VzIFwiOiBbXCJzb21ldGltZXNcIl0sXG4gICAgXCJpbiB0ZXJtcyBvZlwiOiBbXCJvbWl0XCJdLFxuICAgIFwiaW4gdGhlIG5lYXIgZnV0dXJlXCI6IFtcInNvb25cIl0sXG4gICAgXCJpbiB0aGUgcHJvY2VzcyBvZlwiOiBbXCJvbWl0XCJdLFxuICAgIGluY2VwdGlvbjogW1wic3RhcnRcIl0sXG4gICAgXCJpbmN1bWJlbnQgdXBvblwiOiBbXCJtdXN0XCJdLFxuICAgIGluZGljYXRlOiBbXCJzYXlcIiwgXCJzdGF0ZVwiLCBcIm9yIHNob3dcIl0sXG4gICAgaW5kaWNhdGlvbjogW1wic2lnblwiXSxcbiAgICBpbml0aWF0ZTogW1wic3RhcnRcIl0sXG4gICAgXCJpcyBhcHBsaWNhYmxlIHRvXCI6IFtcImFwcGxpZXMgdG9cIl0sXG4gICAgXCJpcyBhdXRob3Jpc2VkIHRvXCI6IFtcIm1heVwiXSxcbiAgICBcImlzIGF1dGhvcml6ZWQgdG9cIjogW1wibWF5XCJdLFxuICAgIFwiaXMgcmVzcG9uc2libGUgZm9yXCI6IFtcImhhbmRsZXNcIl0sXG4gICAgXCJpdCBpcyBlc3NlbnRpYWxcIjogW1wibXVzdFwiLCBcIm5lZWQgdG9cIl0sXG4gICAgbGl0ZXJhbGx5OiBbXCJvbWl0XCJdLFxuICAgIG1hZ25pdHVkZTogW1wic2l6ZVwiXSxcbiAgICBtYXhpbXVtOiBbXCJncmVhdGVzdFwiLCBcImxhcmdlc3RcIiwgXCJtb3N0XCJdLFxuICAgIG1ldGhvZG9sb2d5OiBbXCJtZXRob2RcIl0sXG4gICAgbWluaW1pc2U6IFtcImN1dFwiXSxcbiAgICBtaW5pbWl6ZTogW1wiY3V0XCJdLFxuICAgIG1pbmltdW06IFtcImxlYXN0XCIsIFwic21hbGxlc3RcIiwgXCJzbWFsbFwiXSxcbiAgICBtb2RpZnk6IFtcImNoYW5nZVwiXSxcbiAgICBtb25pdG9yOiBbXCJjaGVja1wiLCBcIndhdGNoXCIsIFwidHJhY2tcIl0sXG4gICAgbXVsdGlwbGU6IFtcIm1hbnlcIl0sXG4gICAgbmVjZXNzaXRhdGU6IFtcImNhdXNlXCIsIFwibmVlZFwiXSxcbiAgICBuZXZlcnRoZWxlc3M6IFtcInN0aWxsXCIsIFwiYmVzaWRlc1wiLCBcImV2ZW4gc29cIl0sXG4gICAgXCJub3QgY2VydGFpblwiOiBbXCJ1bmNlcnRhaW5cIl0sXG4gICAgXCJub3QgbWFueVwiOiBbXCJmZXdcIl0sXG4gICAgXCJub3Qgb2Z0ZW5cIjogW1wicmFyZWx5XCJdLFxuICAgIFwibm90IHVubGVzc1wiOiBbXCJvbmx5IGlmXCJdLFxuICAgIFwibm90IHVubGlrZVwiOiBbXCJzaW1pbGFyXCIsIFwiYWxpa2VcIl0sXG4gICAgbm90d2l0aHN0YW5kaW5nOiBbXCJpbiBzcGl0ZSBvZlwiLCBcInN0aWxsXCJdLFxuICAgIFwibnVsbCBhbmQgdm9pZFwiOiBbXCJ1c2UgZWl0aGVyIG51bGwgb3Igdm9pZFwiXSxcbiAgICBudW1lcm91czogW1wibWFueVwiXSxcbiAgICBvYmplY3RpdmU6IFtcImFpbVwiLCBcImdvYWxcIl0sXG4gICAgb2JsaWdhdGU6IFtcImJpbmRcIiwgXCJjb21wZWxcIl0sXG4gICAgb2J0YWluOiBbXCJnZXRcIl0sXG4gICAgXCJvbiB0aGUgY29udHJhcnlcIjogW1wiYnV0XCIsIFwic29cIl0sXG4gICAgXCJvbiB0aGUgb3RoZXIgaGFuZFwiOiBbXCJvbWl0XCIsIFwiYnV0XCIsIFwic29cIl0sXG4gICAgXCJvbmUgcGFydGljdWxhclwiOiBbXCJvbmVcIl0sXG4gICAgb3B0aW11bTogW1wiYmVzdFwiLCBcImdyZWF0ZXN0XCIsIFwibW9zdFwiXSxcbiAgICBvdmVyYWxsOiBbXCJvbWl0XCJdLFxuICAgIFwib3dpbmcgdG8gdGhlIGZhY3QgdGhhdFwiOiBbXCJiZWNhdXNlXCIsIFwic2luY2VcIl0sXG4gICAgcGFydGljaXBhdGU6IFtcInRha2UgcGFydFwiXSxcbiAgICBwYXJ0aWN1bGFyczogW1wiZGV0YWlsc1wiXSxcbiAgICBcInBhc3MgYXdheVwiOiBbXCJkaWVcIl0sXG4gICAgXCJwZXJ0YWluaW5nIHRvXCI6IFtcImFib3V0XCIsIFwib2ZcIiwgXCJvblwiXSxcbiAgICBcInBvaW50IGluIHRpbWVcIjogW1widGltZVwiLCBcInBvaW50XCIsIFwibW9tZW50XCIsIFwibm93XCJdLFxuICAgIHBvcnRpb246IFtcInBhcnRcIl0sXG4gICAgcG9zc2VzczogW1wiaGF2ZVwiLCBcIm93blwiXSxcbiAgICBwcmVjbHVkZTogW1wicHJldmVudFwiXSxcbiAgICBwcmV2aW91c2x5OiBbXCJiZWZvcmVcIl0sXG4gICAgXCJwcmlvciB0b1wiOiBbXCJiZWZvcmVcIl0sXG4gICAgcHJpb3JpdGlzZTogW1wicmFua1wiLCBcImZvY3VzIG9uXCJdLFxuICAgIHByaW9yaXRpemU6IFtcInJhbmtcIiwgXCJmb2N1cyBvblwiXSxcbiAgICBwcm9jdXJlOiBbXCJidXlcIiwgXCJnZXRcIl0sXG4gICAgcHJvZmljaWVuY3k6IFtcInNraWxsXCJdLFxuICAgIFwicHJvdmlkZWQgdGhhdFwiOiBbXCJpZlwiXSxcbiAgICBwdXJjaGFzZTogW1wiYnV5XCIsIFwic2FsZVwiXSxcbiAgICBcInB1dCBzaW1wbHlcIjogW1wib21pdFwiXSxcbiAgICBcInJlYWRpbHkgYXBwYXJlbnRcIjogW1wiY2xlYXJcIl0sXG4gICAgXCJyZWZlciBiYWNrXCI6IFtcInJlZmVyXCJdLFxuICAgIHJlZ2FyZGluZzogW1wiYWJvdXRcIiwgXCJvZlwiLCBcIm9uXCJdLFxuICAgIHJlbG9jYXRlOiBbXCJtb3ZlXCJdLFxuICAgIHJlbWFpbmRlcjogW1wicmVzdFwiXSxcbiAgICByZW11bmVyYXRpb246IFtcInBheW1lbnRcIl0sXG4gICAgcmVxdWlyZTogW1wibXVzdFwiLCBcIm5lZWRcIl0sXG4gICAgcmVxdWlyZW1lbnQ6IFtcIm5lZWRcIiwgXCJydWxlXCJdLFxuICAgIHJlc2lkZTogW1wibGl2ZVwiXSxcbiAgICByZXNpZGVuY2U6IFtcImhvdXNlXCJdLFxuICAgIHJldGFpbjogW1wia2VlcFwiXSxcbiAgICBzYXRpc2Z5OiBbXCJtZWV0XCIsIFwicGxlYXNlXCJdLFxuICAgIHNoYWxsOiBbXCJtdXN0XCIsIFwid2lsbFwiXSxcbiAgICBcInNob3VsZCB5b3Ugd2lzaFwiOiBbXCJpZiB5b3Ugd2FudFwiXSxcbiAgICBcInNpbWlsYXIgdG9cIjogW1wibGlrZVwiXSxcbiAgICBzb2xpY2l0OiBbXCJhc2sgZm9yXCIsIFwicmVxdWVzdFwiXSxcbiAgICBcInNwYW4gYWNyb3NzXCI6IFtcInNwYW5cIiwgXCJjcm9zc1wiXSxcbiAgICBzdHJhdGVnaXNlOiBbXCJwbGFuXCJdLFxuICAgIHN0cmF0ZWdpemU6IFtcInBsYW5cIl0sXG4gICAgc3Vic2VxdWVudDogW1wibGF0ZXJcIiwgXCJuZXh0XCIsIFwiYWZ0ZXJcIiwgXCJ0aGVuXCJdLFxuICAgIHN1YnN0YW50aWFsOiBbXCJsYXJnZVwiLCBcIm11Y2hcIl0sXG4gICAgXCJzdWNjZXNzZnVsbHkgY29tcGxldGVcIjogW1wiY29tcGxldGVcIiwgXCJwYXNzXCJdLFxuICAgIHN1ZmZpY2llbnQ6IFtcImVub3VnaFwiXSxcbiAgICB0ZXJtaW5hdGU6IFtcImVuZFwiLCBcInN0b3BcIl0sXG4gICAgXCJ0aGUgbW9udGggb2ZcIjogW1wib21pdFwiXSxcbiAgICB0aGVyZWZvcmU6IFtcInRodXNcIiwgXCJzb1wiXSxcbiAgICBcInRoaXMgZGF5IGFuZCBhZ2VcIjogW1widG9kYXlcIl0sXG4gICAgXCJ0aW1lIHBlcmlvZFwiOiBbXCJ0aW1lXCIsIFwicGVyaW9kXCJdLFxuICAgIFwidG9vayBhZHZhbnRhZ2Ugb2ZcIjogW1wicHJleWVkIG9uXCJdLFxuICAgIHRyYW5zbWl0OiBbXCJzZW5kXCJdLFxuICAgIHRyYW5zcGlyZTogW1wiaGFwcGVuXCJdLFxuICAgIFwidW50aWwgc3VjaCB0aW1lIGFzXCI6IFtcInVudGlsXCJdLFxuICAgIHV0aWxpc2F0aW9uOiBbXCJ1c2VcIl0sXG4gICAgdXRpbGl6YXRpb246IFtcInVzZVwiXSxcbiAgICB1dGlsaXNlOiBbXCJ1c2VcIl0sXG4gICAgdXRpbGl6ZTogW1widXNlXCJdLFxuICAgIHZhbGlkYXRlOiBbXCJjb25maXJtXCJdLFxuICAgIFwidmFyaW91cyBkaWZmZXJlbnRcIjogW1widmFyaW91c1wiLCBcImRpZmZlcmVudFwiXSxcbiAgICBcIndoZXRoZXIgb3Igbm90XCI6IFtcIndoZXRoZXJcIl0sXG4gICAgXCJ3aXRoIHJlc3BlY3QgdG9cIjogW1wib25cIiwgXCJhYm91dFwiXSxcbiAgICBcIndpdGggdGhlIGV4Y2VwdGlvbiBvZlwiOiBbXCJleGNlcHQgZm9yXCJdLFxuICAgIHdpdG5lc3NlZDogW1wic2F3XCIsIFwic2VlblwiXVxuICB9O1xufVxuXG5mdW5jdGlvbiBnZXRKdXN0aWZpZXJXb3JkcygpIHtcbiAgcmV0dXJuIHtcbiAgICBcImkgYmVsaWV2ZVwiOiAxLFxuICAgIFwiaSBjb25zaWRlclwiOiAxLFxuICAgIFwiaSBkb24ndCBiZWxpZXZlXCI6IDEsXG4gICAgXCJpIGRvbid0IGNvbnNpZGVyXCI6IDEsXG4gICAgXCJpIGRvbid0IGZlZWxcIjogMSxcbiAgICBcImkgZG9uJ3Qgc3VnZ2VzdFwiOiAxLFxuICAgIFwiaSBkb24ndCB0aGlua1wiOiAxLFxuICAgIFwiaSBmZWVsXCI6IDEsXG4gICAgXCJpIGhvcGUgdG9cIjogMSxcbiAgICBcImkgbWlnaHRcIjogMSxcbiAgICBcImkgc3VnZ2VzdFwiOiAxLFxuICAgIFwiaSB0aGlua1wiOiAxLFxuICAgIFwiaSB3YXMgd29uZGVyaW5nXCI6IDEsXG4gICAgXCJpIHdpbGwgdHJ5XCI6IDEsXG4gICAgXCJpIHdvbmRlclwiOiAxLFxuICAgIFwiaW4gbXkgb3BpbmlvblwiOiAxLFxuICAgIFwiaXMga2luZCBvZlwiOiAxLFxuICAgIFwiaXMgc29ydCBvZlwiOiAxLFxuICAgIGp1c3Q6IDEsXG4gICAgbWF5YmU6IDEsXG4gICAgcGVyaGFwczogMSxcbiAgICBwb3NzaWJseTogMSxcbiAgICBcIndlIGJlbGlldmVcIjogMSxcbiAgICBcIndlIGNvbnNpZGVyXCI6IDEsXG4gICAgXCJ3ZSBkb24ndCBiZWxpZXZlXCI6IDEsXG4gICAgXCJ3ZSBkb24ndCBjb25zaWRlclwiOiAxLFxuICAgIFwid2UgZG9uJ3QgZmVlbFwiOiAxLFxuICAgIFwid2UgZG9uJ3Qgc3VnZ2VzdFwiOiAxLFxuICAgIFwid2UgZG9uJ3QgdGhpbmtcIjogMSxcbiAgICBcIndlIGZlZWxcIjogMSxcbiAgICBcIndlIGhvcGUgdG9cIjogMSxcbiAgICBcIndlIG1pZ2h0XCI6IDEsXG4gICAgXCJ3ZSBzdWdnZXN0XCI6IDEsXG4gICAgXCJ3ZSB0aGlua1wiOiAxLFxuICAgIFwid2Ugd2VyZSB3b25kZXJpbmdcIjogMSxcbiAgICBcIndlIHdpbGwgdHJ5XCI6IDEsXG4gICAgXCJ3ZSB3b25kZXJcIjogMVxuICB9O1xufVxuXG5leHBvcnQge1xuICBmb3JtYXRcbn0iLCJ2YXIgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2Rpc3QvY2pzLmpzIS4vdWkuY3NzXCIpO1xuXG5pZiAodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSB7XG4gIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbn1cblxudmFyIG9wdGlvbnMgPSB7fVxuXG5vcHRpb25zLmluc2VydCA9IFwiaGVhZFwiO1xub3B0aW9ucy5zaW5nbGV0b24gPSBmYWxzZTtcblxudmFyIHVwZGF0ZSA9IHJlcXVpcmUoXCIhLi4vbm9kZV9tb2R1bGVzL3N0eWxlLWxvYWRlci9kaXN0L3J1bnRpbWUvaW5qZWN0U3R5bGVzSW50b1N0eWxlVGFnLmpzXCIpKGNvbnRlbnQsIG9wdGlvbnMpO1xuXG5pZiAoY29udGVudC5sb2NhbHMpIHtcbiAgbW9kdWxlLmV4cG9ydHMgPSBjb250ZW50LmxvY2Fscztcbn1cbiIsImltcG9ydCAnLi91aS5jc3MnO1xuaW1wb3J0IHsgZm9ybWF0IH0gZnJvbSAnLi9oZW1pbmd3YXkuanMnO1xuZnVuY3Rpb24gZGlzcGxheUh0bWwoc2VsZWN0b3IsIHZhbHVlKSB7XG4gICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcihzZWxlY3RvcikuaW5uZXJIVE1MID0gdmFsdWU7XG59XG5mdW5jdGlvbiBnZXRSZWFkYWJpbGl0eUluZGV4KGNoYXJhY3RlcnMsIHdvcmRzLCBzZW50ZW5jZXMpIHtcbiAgICAvLyBBdXRvbWF0ZWQgcmVhZGFiaWxpdHkgaW5kZXhcbiAgICAvLyBodHRwczovL2VuLndpa2lwZWRpYS5vcmcvd2lraS9BdXRvbWF0ZWRfcmVhZGFiaWxpdHlfaW5kZXhcbiAgICByZXR1cm4gTWF0aC5yb3VuZCg0LjcxICogKGNoYXJhY3RlcnMgLyB3b3JkcykgK1xuICAgICAgICAwLjUgKiAod29yZHMgLyBzZW50ZW5jZXMpIC1cbiAgICAgICAgMjEuNDMpO1xufVxuZnVuY3Rpb24gZ2V0UmVhZGluZ0xldmVsKHJlYWRhYmlsaXR5SW5kZXgpIHtcbiAgICBpZiAocmVhZGFiaWxpdHlJbmRleCA8PSA1KSB7XG4gICAgICAgIHJldHVybiBcIlZlcnkgZWFzeVwiO1xuICAgIH1cbiAgICBlbHNlIGlmICg2IDw9IHJlYWRhYmlsaXR5SW5kZXggJiYgcmVhZGFiaWxpdHlJbmRleCA8IDgpIHtcbiAgICAgICAgcmV0dXJuIFwiRWFzeVwiO1xuICAgIH1cbiAgICBlbHNlIGlmICg4IDw9IHJlYWRhYmlsaXR5SW5kZXggJiYgcmVhZGFiaWxpdHlJbmRleCA8IDEyKSB7XG4gICAgICAgIHJldHVybiBcIlN0YW5kYXJkXCI7XG4gICAgfVxuICAgIGVsc2UgaWYgKDEyIDw9IHJlYWRhYmlsaXR5SW5kZXggJiYgcmVhZGFiaWxpdHlJbmRleCA8IDE0KSB7XG4gICAgICAgIHJldHVybiBcIkRpZmZpY3VsdFwiO1xuICAgIH1cbiAgICBlbHNlIGlmICgxNCA8PSByZWFkYWJpbGl0eUluZGV4KSB7XG4gICAgICAgIHJldHVybiBcIlZlcnkgZGlmZmljdWx0XCI7XG4gICAgfVxufVxuZnVuY3Rpb24gZGlzcGxheURhdGEoZGF0YSkge1xuICAgIGRpc3BsYXlIdG1sKFwiI3JlYWRpbmdMZXZlbFwiLCBgJHtnZXRSZWFkaW5nTGV2ZWwoZ2V0UmVhZGFiaWxpdHlJbmRleChkYXRhLmNoYXJhY3RlcnMsIGRhdGEud29yZHMsIGRhdGEuc2VudGVuY2VzKSl9IHJlYWRpbmcgbGV2ZWxgKTtcbiAgICBkaXNwbGF5SHRtbChcIiNjb3VudGVyXCIsIGAke2RhdGEud29yZHN9IHdvcmRzLCAke2RhdGEuY2hhcmFjdGVyc30gY2hhcmFjdGVyc2ApO1xuICAgIGRpc3BsYXlIdG1sKFwiI2FkdmVyYkNvdW50XCIsIGRhdGEuYWR2ZXJicyk7XG4gICAgZGlzcGxheUh0bWwoXCIjYWR2ZXJiTGFiZWxcIiwgYGFkdmVyYiR7ZGF0YS5hZHZlcmJzID4gMSA/IFwic1wiIDogXCJcIn0sICR7ZGF0YS5hZHZlcmJzIDw9IDIgPyBcIm1lZXRpbmcgZ29hbFwiIDogXCJhaW1cIn0gb2YgMiBvciBmZXdlcmApO1xuICAgIGRpc3BsYXlIdG1sKFwiI3Bhc3NpdmVDb3VudFwiLCBkYXRhLnBhc3NpdmVWb2ljZSk7XG4gICAgZGlzcGxheUh0bWwoXCIjcGFzc2l2ZUxhYmVsXCIsIGB1c2Uke2RhdGEucGFzc2l2ZVZvaWNlID4gMSA/IFwic1wiIDogXCJcIn0gb2YgcGFzc2l2ZSB2b2ljZSwgJHtkYXRhLnBhc3NpdmVWb2ljZSA8PSAyID8gXCJtZWV0aW5nIGdvYWxcIiA6IFwiYWltXCJ9IG9mIDIgb3IgZmV3ZXJgKTtcbiAgICBkaXNwbGF5SHRtbChcIiNjb21wbGV4Q291bnRcIiwgZGF0YS5jb21wbGV4KTtcbiAgICBkaXNwbGF5SHRtbChcIiNjb21wbGV4TGFiZWxcIiwgXCJwaHJhc2VzIGhhdmUgYSBzaW1wbGVyIGFsdGVybmF0aXZlc1wiKTtcbiAgICBkaXNwbGF5SHRtbChcIiNoYXJkU2VudGVuY2VDb3VudFwiLCBkYXRhLmhhcmRTZW50ZW5jZXMpO1xuICAgIGRpc3BsYXlIdG1sKFwiI3ZlcnlIYXJkU2VudGVuY2VDb3VudFwiLCBkYXRhLnZlcnlIYXJkU2VudGVuY2VzKTtcbn1cbm9ubWVzc2FnZSA9IChldmVudCkgPT4ge1xuICAgIGNvbnN0IHRleHQgPSBldmVudC5kYXRhLnBsdWdpbk1lc3NhZ2U7XG4gICAgY29uc3QgdGV4dE91dHB1dCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCd0ZXh0T3V0cHV0Jyk7XG4gICAgLy8gVE9ETyBwcm9jZXNzIGFuZCBoaWdobGlnaHQgdGV4dCBhbmQgZ2V0IEhlbWluZ3dheSBjb3VudHNcbiAgICBjb25zdCBhbmFseXNpcyA9IGZvcm1hdCh0ZXh0KTtcbiAgICB0ZXh0T3V0cHV0LmlubmVySFRNTCA9IGFuYWx5c2lzLm1hcmt1cDtcbiAgICBkaXNwbGF5RGF0YShhbmFseXNpcy5zdGF0cyk7XG4gICAgLy8gR2V0IGhlaWdodCB0byByZXNpemUgdGhlIG1vZGFsXG4gICAgY29uc3QgY29udGFpbmVyID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2hlbWluZ3dheScpO1xuICAgIGNvbnN0IHJlc2l6ZUhlaWdodCA9IE1hdGgubWluKDYxMCwgY29udGFpbmVyLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpLmhlaWdodCArIDQwKTtcbiAgICBwYXJlbnQucG9zdE1lc3NhZ2UoeyBwbHVnaW5NZXNzYWdlOiB7IHR5cGU6ICdyZXNpemUnLCBoZWlnaHQ6IHJlc2l6ZUhlaWdodCB9IH0sICcqJyk7XG59O1xuIl0sInNvdXJjZVJvb3QiOiIifQ==