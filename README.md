# Hemingway for Figma
Make your writing bold and clear.

Select any text element and run the plugin. It will analyse the text using Hemingway rules around adverbs, passive voices, complex phrases, and hard sentences.

The plugin will also check word count, character count, and assess reading level using the automated readability index.

This plugin was inspired by the Hemingway Editor. It is not an official plugin from that team

## Installation link
https://www.figma.com/c/plugin/760035865558407437/Hemingway

## Developer notes
Using Visual Studio Code you can run the commands `tsc watch` and `tsc build` to run the code. 

More details https://www.figma.com/plugin-docs/setup/
